''' decision_maker provides simple decision makers for the simulation '''

import copy as copy
from flow_simulation.classes.agent import *
from flow_simulation.classes.decision import *


class DecisionMaker(object):
    ''' Decision Maker makes decisions for all the agents '''

    def make_decision(self, now):
        ''' make decisions for all the agents'''
        pass


class PerAgentDecisionMaker(DecisionMaker):
    ''' PerAgentDecisionMaker is a decision maker that combines all the
        agent level decision makers.
    '''

    def __init__(self):
        self.decision_makers = []

    def add_decision_maker(self, decision_maker):
        ''' Register a agent level decision maker'''
        self.decision_makers.append(decision_maker)

    def make_decision(self, now):
        for decision_maker in self.decision_makers:
            decision_maker.make_decision(now)


# The treatment policy should change
class UrgentFirstHCDecisionMaker(DecisionMaker):
    def __init__(self, hc):
        self.hc = hc

    def make_decision(self, now):
        num_upstream_node = len(self.hc.upstream_nodes)
        total_inventory = sum(tempIn.amount for tempIn in self.hc.inventory)
        total_on_order = sum(tempO.amount for tempO in self.hc.on_order)
        total_demand = self.hc.urgent + self.hc.non_urgent

        # Treat decision
        treat_decision = TreatDecision()
        if total_inventory < total_demand:
            treat_decision.non_urgent = int(
                total_inventory * (float(self.hc.non_urgent) / total_demand))
            treat_decision.urgent = total_inventory - treat_decision.non_urgent
            self.hc.decisions.append(treat_decision)
        else:
            treat_decision.non_urgent = self.hc.non_urgent
            treat_decision.urgent = self.hc.urgent
            self.hc.decisions.append(treat_decision)

        # Order decision
        total_inventory = total_inventory - \
                          treat_decision.urgent - treat_decision.non_urgent
        order_amount = self.hc.up_to_level - total_inventory - total_on_order

        if order_amount < 0:
            order_amount = 0
        for agent in self.hc.upstream_nodes:
            order_decision = OrderDecision()
            order_decision.upstream = agent
            order_decision.amount = int(
                float(order_amount) / num_upstream_node)
            self.hc.decisions.append(order_decision)


class SimpleHCDecisionMaker(DecisionMaker):
    def __init__(self, hc, split_recipe):
        self.hc = hc
        self.split_recipe = split_recipe

    def make_decision(self, now):
        totalInv = sum(tempIn.amount for tempIn in self.hc.inventory)
        totalOnOrder = sum(tempO.amount for tempO in self.hc.on_order)  # I should add the backlog to this
        totalBacklog_non_urgent = self.hc.backlog_non_urgent + self.hc.non_urgent  # Backlog for non-urgent
        totalDemand = self.hc.urgent + totalBacklog_non_urgent

        # update trust
        delivery_amnt = {}
        for up_agent in self.hc.upstream_nodes:
            delivery_amnt[up_agent] = 0
        delivery = self.hc.get_history_item(now)['delivery']
        for deliv in delivery:
            delivery_amnt[deliv['src']] += deliv['item'].amount

        for up_agent in self.hc.upstream_nodes:
            if not bool(self.hc.expctd_on_order) or self.hc.expctd_on_order[up_agent][0] == 0:
                self.hc.ontime_deliv_rate[up_agent] = 1
            else:
                self.hc.ontime_deliv_rate[up_agent] = delivery_amnt[up_agent] / self.hc.expctd_on_order[up_agent][0]

        if not bool(self.hc.trust):
            for up_agent in self.hc.upstream_nodes:
                self.hc.trust[up_agent] = 1
        else:
            for up_agent in self.hc.upstream_nodes:
                self.hc.trust[up_agent] = (1 - self.hc.delta) * self.hc.trust[up_agent] + \
                                          self.hc.delta * self.hc.ontime_deliv_rate[up_agent]

        decisionT = TreatDecision()
        if totalInv < totalDemand:
            decisionT.non_urgent = int(
                totalInv * (float(totalBacklog_non_urgent) / (totalDemand)))
            decisionT.urgent = int(
                totalInv * (float(self.hc.urgent) / totalDemand))
            self.hc.decisions.append(decisionT)
        else:
            decisionT.non_urgent = int(totalBacklog_non_urgent)
            decisionT.urgent = int(self.hc.urgent)
            self.hc.decisions.append(decisionT)

        # update residual inventory used later for calculating order amount
        self.res_inventory = totalInv - decisionT.urgent - decisionT.non_urgent

        # order decision  ## I should update this
        totalBacklog_non_urgent -= decisionT.non_urgent  #
        orderAmount = max(self.hc.up_to_level -
                          totalOnOrder - self.res_inventory + totalBacklog_non_urgent, 0)

        if self.split_recipe == 'equally':
            # Order Equally Recipe
            noUptr = len(self.hc.upstream_nodes)
            for agent in self.hc.upstream_nodes:
                decisionO = OrderDecision()
                decisionO.upstream = agent
                decisionO.amount = int(float(orderAmount) / noUptr)
                self.hc.decisions.append(decisionO)
        elif self.split_recipe == 'bytrust':
            sum_trust = 0
            for up_agent in self.hc.upstream_nodes:
                sum_trust += self.hc.trust[up_agent]
            for up_agent in self.hc.upstream_nodes:
                decisionO = OrderDecision()
                decisionO.upstream = up_agent
                decisionO.amount = int(float(orderAmount * self.hc.trust[up_agent]) / sum_trust)
                self.hc.decisions.append(decisionO)
        else:
            raise ValueError("Split Order Recipe Is Not Defined")


class SimpleDSDecisionMaker(DecisionMaker):
    def __init__(self, ds):
        self.ds = ds

    def make_decision(self, now):
        inventory = self.ds.inventory_level()
        # allocation decision
        allocated = allocate_proportional(self.ds)
        # allocated = allocate_equally(self.ds)
        inventory -= allocated

        # order decision
        on_order = sum(tempO.amount for tempO in self.ds.on_order)
        backlog = self.ds.backlog_level()
        backlog -= allocated
        order_amount = max(self.ds.up_to_level + backlog -
                           on_order - inventory, 0)

        # Split Order Equally Recipe
        num_upstream_nodes = len(self.ds.upstream_nodes)
        for agent in self.ds.upstream_nodes:
            decision = OrderDecision()
            decision.upstream = agent
            decision.amount = int(float(order_amount) / num_upstream_nodes)
            self.ds.decisions.append(decision)

        # todo Order By Trust


class TempMNDecisionMaker(DecisionMaker):
    def __init__(self, mn):
        self.mn = mn

    def make_decision(self, now):
        inventory = self.mn.inventory_level()

        # allocation decision
        # each time choose one of these to test the recipe
        allocated = allocate_proportional(self.mn)
        # allocated = allocate_equally(self.mn)
        inventory -= allocated

        # production Decision
        available_capacity = self.mn.num_active_lines * self.mn.line_capacity
        in_production = sum(
            in_prod.amount for in_prod in self.mn.in_production)
        in_backlog = self.mn.backlog_level()
        in_backlog -= allocated

        if now < 120:
            # use up-to level to calculate production amount
            prod_amount = self.mn.up_to_level + in_backlog - inventory - in_production
            if prod_amount < 0:
                prod_amount = 0
            elif prod_amount > available_capacity:
                prod_amount = available_capacity  # can't produce more than available capacity
        elif now < 140:
            prod_amount = 200
        elif now < 160:
            prod_amount = 60
        else:
            prod_amount = 120
            # # use up-to level to calculate production amount
            # prod_amount = self.mn.up_to_level + in_backlog - inventory - in_production
            # if prod_amount < 0:
            #     prod_amount = 0
            # elif prod_amount > available_capacity:
            #     prod_amount = available_capacity  # can't produce more than available capacity

        # adjust the production amount according to line capacity (when a line is open it works at its full capacity)
        line_cap = self.mn.line_capacity
        prod_amount = np.ceil(prod_amount / line_cap) * line_cap
        self.mn.prod_amount = prod_amount

        decision_p = ProduceDecision()
        decision_p.amount = prod_amount
        self.mn.decisions.append(decision_p)


class SimpleMNDecisionMaker(DecisionMaker):
    def __init__(self, mn):
        self.mn = mn

    def make_decision(self, now):
        inventory = self.mn.inventory_level()

        # allocation decision
        # each time choose one of these to test the recipe
        allocated = allocate_proportional(self.mn)
        # allocated = allocate_equally(self.mn)
        inventory -= allocated

        # production Decision
        available_capacity = self.mn.num_active_lines * self.mn.line_capacity
        in_production = sum(
            in_prod.amount for in_prod in self.mn.in_production)
        in_backlog = self.mn.backlog_level()
        in_backlog -= allocated
        # use up-to level to calculate production amount
        prod_amount = self.mn.up_to_level + in_backlog - inventory - in_production
        if prod_amount < 0:
            prod_amount = 0
        elif prod_amount > available_capacity:
            prod_amount = available_capacity  # can't produce more than available capacity

        # adjust the production amount according to line capacity (when a line is open it works at its full capacity)
        line_cap = self.mn.line_capacity
        prod_amount = np.ceil(prod_amount / line_cap) * line_cap
        self.mn.prod_amount = prod_amount

        decision_p = ProduceDecision()
        decision_p.amount = prod_amount
        self.mn.decisions.append(decision_p)


def allocate_proportional(agent):
    backlog = agent.backlog_level()
    inventory = agent.inventory_level()
    allocated = 0
    if inventory == 0 or backlog == 0:
        return 0

    demand_of = {}
    for a in agent.downstream_nodes:
        demand_of[a] = 0

    for bl in agent.backlog:
        if not bl.src in demand_of:
            demand_of[bl.src] = 0
        demand_of[bl.src] += bl.amount

    allocate_to = {}
    for ag in agent.downstream_nodes:
        allocate_to[ag] = int(min(demand_of[ag],
                                  (float(demand_of[ag] * inventory) / backlog)))

    allocated = sum(allocate_to.values())

    inv_ptr = 0
    inv_left = agent.inventory[0].amount
    for bl in agent.backlog:
        bl_left = bl.amount
        while allocate_to[bl.src] > 0 and bl_left > 0:
            if min(allocate_to[bl.src], bl_left) <= inv_left:
                decision_al = AllocateDecision()
                decision_al.amount = min(allocate_to[bl.src], bl_left)
                allocate_to[bl.src] -= decision_al.amount
                bl_left -= decision_al.amount
                inv_left -= decision_al.amount
                decision_al.item = agent.inventory[inv_ptr]
                decision_al.order = bl
                agent.decisions.append(decision_al)
            else:
                decision_al = AllocateDecision()
                decision_al.amount = inv_left
                allocate_to[bl.src] -= decision_al.amount
                bl_left -= decision_al.amount
                decision_al.item = agent.inventory[inv_ptr]
                decision_al.order = bl
                agent.decisions.append(decision_al)
                inv_ptr += 1
                inv_left = agent.inventory[inv_ptr].amount

    return allocated


def allocate_equally(agent):
    inventory = agent.inventory_level()
    backlog = agent.backlog_level()
    allocated = 0

    if inventory == 0 or backlog == 0:
        return 0

    num_downstream = len(agent.downstream_nodes)
    demand_of = {}  # dictionary of how much total demand each downstream node has
    for a in agent.downstream_nodes:
        demand_of[a] = 0

    for bl in agent.backlog:
        if not bl.src in demand_of:
            demand_of[bl.src] = 0
        demand_of[bl.src] += bl.amount

    allocate_to = {}
    for ag in agent.downstream_nodes:
        allocate_to[ag] = min(
            demand_of[ag], (inventory / num_downstream))

    allocated = sum(allocate_to.values())

    inv_ptr = 0
    inv_left = agent.inventory[0].amount
    for bl in agent.backlog:
        bl_left = bl.amount
        while allocate_to[bl.src] > 0 and bl_left > 0:
            if min(allocate_to[bl.src], bl_left) <= inv_left:
                decision_al = AllocateDecision()
                decision_al.amount = min(allocate_to[bl.src], bl_left)
                allocate_to[bl.src] -= decision_al.amount
                bl_left -= decision_al.amount
                inv_left -= decision_al.amount
                decision_al.item = agent.inventory[inv_ptr]
                decision_al.order = bl
                agent.decisions.append(decision_al)
            else:
                decision_al = AllocateDecision()
                decision_al.amount = inv_left
                allocate_to[bl.src] -= decision_al.amount
                bl_left -= decision_al.amount
                decision_al.item = agent.inventory[inv_ptr]
                decision_al.order = bl
                agent.decisions.append(decision_al)
                inv_ptr += 1
                inv_left = agent.inventory[inv_ptr].amount

    return allocated
