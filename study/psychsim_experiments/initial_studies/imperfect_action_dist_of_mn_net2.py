from flow_simulation.classes.disruption import LineShutDownDisruption
from study.psychsim_experiments import *

ALLOCATE_PROPORTIONAL = 'alloc_prop'
ALLOCATE_EQUALLY = 'alloc_eq'


class ImperfectActionDistMnNet2(SimulationProfileBase222):
    """
    A simulation profile for testing the effect of distributor(s) having a false model of disrupted manufacturer's
    available allocation recipe.
    """

    def __init__(self, name, dist_false_model_idxs, dist_planning_idxs, mn_true_recipe=ALLOCATE_PROPORTIONAL):
        """
        Creates and configures the profile.
        :param str name: the name of this profile (used to create results folder).
        :param list dist_false_model_idxs: a list with the indexes of the distributors having the false model of the
        manufacturer's allocation recipe.
        :param list dist_planning_idxs: a list with the indexes of the distributors with planning capacity.
        :param str mn_true_recipe: the id of the allocation recipe for the manufacturer (true model).
        """
        self.dist_false_model_idxs = dist_false_model_idxs
        self.dist_planning_idxs = dist_planning_idxs
        self.mn_true_recipe = mn_true_recipe
        super(ImperfectActionDistMnNet2, self).__init__(name)

    def define_agent_connections(self):
        # sets default full connectivity
        super(ImperfectActionDistMnNet2, self).define_agent_connections()

        # DS2 is connected only to MN2 and hence MN1 is only connected to DS1.
        self.ds2.upstream_nodes = [self.mn2]
        self.mn1.downstream_nodes = [self.ds1]

    def parameterize_sim_agents(self):
        # default parametrization up-to level for 2X2X2 complete network
        super(ImperfectActionDistMnNet2, self).parameterize_sim_agents()

        # different up-to-levels for manufacturers
        self.mn1.up_to_level = 120
        self.mn2.up_to_level = 360

    def add_disruptions(self):
        # manufacturer shutdown with custom period
        manufacturer_shutdown = LineShutDownDisruption(self.simulation, 40)
        manufacturer_shutdown.manufacturer_id = 1
        manufacturer_shutdown.happen_day_1 = 20
        manufacturer_shutdown.end_day_1 = 30
        manufacturer_shutdown.decrease_factor_1 = 0.625
        self.simulation.disruptions.append(manufacturer_shutdown)

    def add_distributors_recipes(self):
        # sets up ordering recipes (both distributors can opt between ordering recipe)
        order_offset = 0.2
        order_up_to = DistributorUpToOrderAmountRecipe('1ord_up')
        order_more = DistributorUpToOrderAmountRecipe('ord+', order_offset)
        order_less = DistributorUpToOrderAmountRecipe('ord-', -order_offset)
        order_amt_recipes_planning = [order_up_to, order_less, order_more]
        order_amt_recipes_no_planning = [order_up_to]

        # gets distributors with planning capacity
        planning_ds = []
        for idx in self.dist_planning_idxs:
            planning_ds.append(self.simulation.distributors[idx])

        # adds recipes
        for ds in self.simulation.distributors:
            order_amt_recipes = order_amt_recipes_no_planning
            if ds in planning_ds:
                order_amt_recipes = order_amt_recipes_planning

            available_recipes = {
                ALLOCATION: [DistributorAllocateProportionalRecipe('alloc_prop')],
                # FORECAST_DEMAND: [DistributorDemandForecastRecipe('dem_fore')],
                # CALCULATE_UP_TO_LEVEL: [DistributorUpToLevelRecipe('calc_up')],
                ORDER_AMOUNT: order_amt_recipes,
                ORDERING_SPLIT: [DistributorOrderSplitEquallyRecipe('split_eq')],
                TRUST: [DistributorUpdateTrustByHistoryRecipe('upd_trust_hist', 0.8)]
            }
            # creates state-action dynamics
            agent = self.agent_converter.get_psychsim_agent(ds)
            agent.define_state_dynamics(available_recipes)

    def add_manufacturers_recipes(self):
        # sets up allocation recipes
        if self.mn_true_recipe == ALLOCATE_PROPORTIONAL:
            true_allocate = ManufacturerAllocateProportionalRecipe(ALLOCATE_PROPORTIONAL)
            false_allocate = ManufacturerAllocateEquallyRecipe(ALLOCATE_EQUALLY)
        else:
            true_allocate = ManufacturerAllocateEquallyRecipe(ALLOCATE_EQUALLY)
            false_allocate = ManufacturerAllocateProportionalRecipe(ALLOCATE_PROPORTIONAL)

        for mn in self.simulation.manufacturers:

            # only MN2 will have one two recipes
            if mn == self.mn2:
                allocation_recipes = [true_allocate, false_allocate]
            else:
                allocation_recipes = [ManufacturerAllocateProportionalRecipe(ALLOCATE_PROPORTIONAL)]

            available_recipes = {
                ALLOCATION: allocation_recipes,
                FORECAST_DEMAND: [ManufacturerDemandForecastRecipe('dem_fore')],
                CALCULATE_UP_TO_LEVEL: [ManufacturerUpToLevelRecipe('calc_up')],
                PRODUCTION_AMOUNT: [ManufacturerUpToProductionRecipe('prod_pro')]
            }

            # creates state-action dynamics
            agent = self.agent_converter.get_psychsim_agent(mn)
            agent.define_state_dynamics(available_recipes)

    def add_mental_models(self):

        # gets distributors with false model of MN2
        false_model_ds = []
        for idx in self.dist_false_model_idxs:
            false_model_ds.append(self.agent_converter.get_psychsim_agent(self.simulation.distributors[idx]).ps_agent)

        # adds false model only to MN2
        mn2 = self.agent_converter.get_psychsim_agent(self.mn2).ps_agent
        mn2.addModel(FALSE_MODEL)

        for agent in self.world.agents.values():
            # adds a copy of the true model for all agents
            agent.addModel(TRUE_MODEL)

            # adds respective model to all other agents
            for other in self.world.agents.values():
                if other != agent:
                    # some distributors have false model of MN2
                    if agent == mn2 and other in false_model_ds:
                        self.world.setMentalModel(other.name, agent.name, {FALSE_MODEL: 1})
                    else:
                        self.world.setMentalModel(other.name, agent.name, {TRUE_MODEL: 1})

    def add_action_beliefs(self):
        # sets illegal actions for MN2 models according to allocation recipe
        mn2 = self.agent_converter.get_psychsim_agent(self.mn2).ps_agent
        for action in mn2.actions:
            if self.mn_true_recipe in action['verb']:
                set_illegal_action(mn2, action, [True, TRUE_MODEL])
            else:
                set_illegal_action(mn2, action, [FALSE_MODEL])
