from psychsim_agents.psychsim_cleaning_agent import PSCleaningAgent
from psychsim_agents.psychsim_distributor import *
from psychsim_agents.psychsim_health_center import *
from psychsim_agents.psychsim_manufacturer import *
from psychsim_agents.constants import *
from flow_simulation.classes.network import *
from flow_simulation.classes.patient_model import *
from flow_simulation.classes.simulation import *
from psychsim.world import World
from psychsim.reward import *

MANUFACTURER_PREFIX = 'MN '
DISTRIBUTOR_PREFIX = 'DS '
HEALTH_CENTER_PREFIX = 'HC '

CLEANING_AG_ID = 'cleaning'
HEALTH_CENTER_ID_STR = "health_center"
DISTRIBUTOR_ID_STR = "distributor"
MANUFACTURER_ID_STR = "manufacturer"


class SimulationProfileBase(object):
    """
    A Simulation profile defines all the parameters of a simulation for some study / experiment.
    """

    def __init__(self, name, n_mn=1, n_ds=1, n_hc=1):
        """
        Creates and configures the profile.
        :param str name: the name of this profile (used to create results folder).
        :param int n_mn: the number of manufacturers in the simulation.
        :param int n_ds: the number of distributors in the simulation.
        :param int n_hc: the number of health centers in the simulation.
        """

        self.name = name

        # sets default parameters
        self.time_periods = 50

        # creates the main simulation
        self.simulation = Simulation()

        # creates agent builder and sets params
        self.agent_builder = AgentBuilder()
        self.parameterize_agent_builder()

        # creates agents for simulation
        self.create_sim_agents(n_mn, n_ds, n_hc)

        # creates simulation network
        self.create_network()

        # defines all network connectivity
        self.define_agent_connections()

        # parametrizes all simulation agents
        self.parameterize_sim_agents()

        # adds patient and disruption profiles
        self.add_patient_model()
        self.add_disruptions()

        # create and parametrize PsychSim world
        self.world = World()
        self.parameterize_world()

        # create PsychSim agents
        self.agent_converter = AgentConverter()
        self.cleaning_agent = None
        self.create_psychsim_agents()

        # parametrize and initializes PsychSim agents
        self.parameterize_psychsim_agents()
        for agent in self.get_all_psychsim_agents():
            agent.create_state_variables()
            agent.define_dependencies()
        # reward
        self.add_reward_function()

        # defines state-action dynamics for all PsychSim agents
        self.add_health_centers_recipes()
        self.add_manufacturers_recipes()
        self.add_distributors_recipes()
        self.add_cleaning_agent_recipes()

        # adds PsychSim agents mental models
        self.add_mental_models()

        # adds PsychSim beliefs
        self.add_perceptual_beliefs()
        self.add_action_beliefs()

    def get_all_psychsim_agents(self):
        """
        Gets a list containing all PsychSimAgent used in this experiment.
        :return: a list containing all PsychSimAgent used in this experiment.
        :rtype list
        """
        all_ag = self.agent_converter.all_psychsim_agents()
        all_ag.append(self.cleaning_agent)
        return all_ag

    def parameterize_agent_builder(self):
        """
        Sets the default parameters for the agent builder.
        """
        self.agent_builder.lead_time = 2
        self.agent_builder.review_time = 0
        self.agent_builder.cycle_service_level = 0.9
        self.agent_builder.history_preserve_time = 20
        self.agent_builder.use_fixed_order_up_to_level()

    def create_sim_agents(self, n_mn, n_ds, n_hc):
        """
        Creates all simulation agents needed for this experiment.
        :param n_mn: the number of manufacturers in the simulation.
        :param n_ds: the number of distributors in the simulation.
        :param n_hc: the number of health centers in the simulation.
        """
        for i in range(n_mn):
            self.simulation.add_agent(self.agent_builder.build(MANUFACTURER_ID_STR))
        for i in range(n_ds):
            self.simulation.add_agent(self.agent_builder.build(DISTRIBUTOR_ID_STR))
        for i in range(n_hc):
            self.simulation.add_agent(self.agent_builder.build(HEALTH_CENTER_ID_STR))

    def create_network(self):
        """
        By default, creates a fully-connected network for the simulation according to the number of agents.
        """
        num_agents = len(self.simulation.agents)
        net = Network(num_agents)
        info_net = Network(num_agents)
        for i in range(num_agents):
            for j in range(num_agents):
                net.connectivity[i, j] = 1
                info_net.connectivity[i, j] = 0
        self.simulation.network = net
        self.simulation.info_network = info_net

    def define_agent_connections(self):
        """
        Define the up/downstream nodes for all the agents in the network. Default is everyone is connected.
        """
        for ds in self.simulation.distributors:

            # ds <-> hc
            for hc in self.simulation.health_centers:
                hc.upstream_nodes.append(ds)
                ds.downstream_nodes.append(hc)

            # ds <-> mn
            for mn in self.simulation.manufacturers:
                ds.upstream_nodes.append(mn)
                mn.downstream_nodes.append(ds)

    def parameterize_sim_agents(self):
        """
        Parametrizes all simulation agents, eg production amounts, up-to levels, etc. Default is 240 for up-to level,
        40 lines of 10 for manufacturers. Also initializes on-order and in-transits vars.
        """

        # initializes on-order and in-transit
        for hc in self.simulation.health_centers:
            hc.in_transit_inventory = {}
            for upst in hc.upstream_nodes:
                l_time = self.simulation.network.connectivity[upst.id, hc.id]
                hc.in_transit_inventory[upst] = np.zeros(l_time)
                hc.expctd_on_order[upst] = np.zeros(l_time)
        for ds in self.simulation.distributors:
            ds.in_transit_inventory = {}
            for upst in ds.upstream_nodes:
                l_time = self.simulation.network.connectivity[upst.id, ds.id]
                ds.in_transit_inventory[upst] = np.zeros(l_time)
                ds.expctd_on_order[upst] = np.zeros(l_time)
        for mn in self.simulation.manufacturers:
            mn.in_transit_inventory[mn] = np.zeros(mn.lead_time)

        # sets default up-to level
        for agent in self.simulation.agents:
            agent.up_to_level = 120

        # sets default production cap. for manufacturers
        for mn in self.simulation.manufacturers:
            mn.num_of_lines = 40
            mn.line_capacity = 10
            mn.num_active_lines = 40

        # lead time dictionary for PsychSim
        connect = self.simulation.network.connectivity
        for hc in self.simulation.health_centers:
            for upst in hc.upstream_nodes:
                hc.lead_time_dict[upst] = connect[upst.id, hc.id]
        for ds in self.simulation.distributors:
            for upst in ds.upstream_nodes:
                ds.lead_time_dict[upst] = connect[upst.id, ds.id]
        for mn in self.simulation.manufacturers:
            mn.lead_time_dict[mn] = mn.lead_time

        # initialize on-time delivery rate
        for hc in self.simulation.health_centers:
            for upst in hc.upstream_nodes:
                hc.ontime_deliv_rate[upst] = 1

        for ds in self.simulation.distributors:
            for upst in ds.upstream_nodes:
                ds.ontime_deliv_rate[upst] = 1

    def add_disruptions(self):
        """
        Adds possible disruption profiles.
        Default in no disruption.
        """
        pass

    def add_patient_model(self):
        """
        Adds one patient model to generate demand at the health centers.
        Default is a constant patient model for all health centers in the network.
        """
        patient_model = ConstantPatientModel(self.simulation.health_centers)
        self.simulation.patient_model = patient_model

    def parameterize_world(self):
        """
        Sets default parameters to PsychSim world.
        """
        self.world.memory = False  # False
        # self.world.memory = False


    def create_psychsim_agents(self):
        """
        Creates the agents connecting to PsychSim. Does not initialize (create variables, actions) the agents.
        Default is one PsychSim agent per simulation agent plus one 'cleaning' agent.
        Default turn order is: 1) all agents decide in parallel, 2) cleaning agent updates
        """

        # create a PsychSim agent or each simulation agent
        turn_order = set()
        idx = 1
        for hc in self.simulation.health_centers:
            name = HEALTH_CENTER_PREFIX + str(idx)
            agent = PSHealthCenterAgent(self.world, self.agent_converter, name)
            self.agent_converter.add(hc, agent)
            turn_order.add(name)
            idx += 1

        idx = 1
        for ds in self.simulation.distributors:
            name = DISTRIBUTOR_PREFIX + str(idx)
            agent = PSDistributorAgent(self.world, self.agent_converter, name)
            self.agent_converter.add(ds, agent)
            turn_order.add(name)
            idx += 1

        idx = 1
        for mn in self.simulation.manufacturers:
            name = MANUFACTURER_PREFIX + str(idx)
            agent = PSManufacturerAgent(self.world, self.agent_converter, name)
            self.agent_converter.add(mn, agent)
            turn_order.add(name)
            idx += 1

        # creates one cleaning agent
        self.cleaning_agent = PSCleaningAgent(self.world, self.agent_converter, CLEANING_AG_ID)

        # sets turn order: all agents are in a set (parallel), cleaning agent updates at the end
        self.world.setOrder([turn_order, CLEANING_AG_ID])

    def parameterize_psychsim_agents(self):
        """
        Parametrizes all psychsim agents.
        Default is all agents have a 6 period look-ahead with a discount of 1. Sets default values for other agents.
        """

        # default parameters for all agents
        for agent in self.agent_converter.all_psychsim_agents():
            agent.ps_agent.setHorizon(6)  # Horizon is how far ahead agent reasons to determine next action
            agent.ps_agent.setRecursiveLevel(1)
            agent.ps_agent.setAttribute('discount', 1.0)  # Discount is how much agent discounts future rewards
            # agent.ps_agent.setAttribute('discretization', 10)  # sets the discretization level / number of groups
            agent.smoothing_constant = 0.8

        # default parameters for health centers
        for hc in self.simulation.health_centers:
            agent = self.agent_converter.get_psychsim_agent(hc)
            agent.totals = {INV_HI: 400, UP_TO_HI: 400, UP_STR_HI: 3, ORDER_HI: 400, DEM_HI: 400, CUR_DEM_HI: 400,
                            LEAD_TIME_HI: 4}

        # default parameters for manufacturers
        for mn in self.simulation.manufacturers:
            agent = self.agent_converter.get_psychsim_agent(mn)
            agent.totals = {INV_HI: 400, UP_TO_HI: 400, DN_STR_HI: 3, ORDER_HI: 400, DEM_HI: 400, PROD_HI: 400,
                            LINE_HI: 10, BL_HI: 400, CUR_DEM_HI: 400, LEAD_TIME_HI: 3}

        # default parameters for distributors
        for ds in self.simulation.distributors:
            agent = self.agent_converter.get_psychsim_agent(ds)
            agent.totals = {INV_HI: 400, UP_TO_HI: 400, UP_STR_HI: 3, DN_STR_HI: 3, ORDER_HI: 400, DEM_HI: 400,
                            BL_HI: 400, CUR_DEM_HI: 400, LEAD_TIME_HI: 3}

        # default parameters for cleaning agent
        self.cleaning_agent.totals = {ORDER_HI: 1000}

    def add_reward_function(self):
        """

        :return:
        """
        # set the reward function for manufacturers
        mn1 = self.agent_converter.get_psychsim_agent(self.mn1)
        mn1.tot_bklog_after_alloc_rwd_weight = 10.0
        mn1.inv_after_alloc_rwd_weight = 1.0
        mn1.allocate_rwd_weight = 5.0

        mn1.ps_agent.setReward(minimizeFeature(mn1.inv_after_allocation), mn1.inv_after_alloc_rwd_weight)
        mn1.ps_agent.setReward(
            minimizeFeature(mn1.total_backlog_after_allocation), mn1.tot_bklog_after_alloc_rwd_weight)
        for d in mn1.down_stream_agents:
            mn1.ps_agent.setReward(maximizeFeature(mn1.allocate_to[d]), mn1.allocate_rwd_weight)

        mn2 = self.agent_converter.get_psychsim_agent(self.mn2)
        mn2.tot_bklog_after_alloc_rwd_weight = 10.0
        mn2.inv_after_alloc_rwd_weight = 1.0
        mn2.allocate_rwd_weight = 5.0

        mn2.ps_agent.setReward(minimizeFeature(mn2.inv_after_allocation), mn2.inv_after_alloc_rwd_weight)
        mn2.ps_agent.setReward(
            minimizeFeature(mn2.total_backlog_after_allocation), mn2.tot_bklog_after_alloc_rwd_weight)
        for d in mn2.down_stream_agents:
            mn2.ps_agent.setReward(maximizeFeature(mn2.allocate_to[d]), mn2.allocate_rwd_weight)

        # set the reward function of the distributors
        ds1 = self.agent_converter.get_psychsim_agent(self.ds1)
        ds1.tot_bklog_after_alloc_rwd_weight = 10.0
        ds1.inv_after_alloc_rwd_weight = 1.0
        ds1.allocate_rwd_weight = 5.0

        ds1.ps_agent.setReward(minimizeFeature(ds1.inv_after_allocation), ds1.inv_after_alloc_rwd_weight)
        ds1.ps_agent.setReward(
            minimizeFeature(ds1.total_backlog_after_allocation), ds1.tot_bklog_after_alloc_rwd_weight)
        for d in ds1.down_stream_agents:
            ds1.ps_agent.setReward(maximizeFeature(ds1.allocate_to[d]), ds1.allocate_rwd_weight)

        ds2 = self.agent_converter.get_psychsim_agent(self.ds2)
        ds2.tot_bklog_after_alloc_rwd_weight = 10.0
        ds2.inv_after_alloc_rwd_weight = 1.0
        ds2.allocate_rwd_weight = 5.0

        ds2.ps_agent.setReward(minimizeFeature(ds2.inv_after_allocation), ds2.inv_after_alloc_rwd_weight)
        ds2.ps_agent.setReward(
            minimizeFeature(ds2.total_backlog_after_allocation), ds2.tot_bklog_after_alloc_rwd_weight)
        for d in ds2.down_stream_agents:
            ds2.ps_agent.setReward(maximizeFeature(ds2.allocate_to[d]), ds2.allocate_rwd_weight)

        # set reward for health-centers
        hc1 = self.agent_converter.get_psychsim_agent(self.hc1)
        hc1.inv_rwd_weight = 1.0
        hc1.lost_urgent_rwd_weight = 100.0
        hc1.lost_non_urgent_rwd_weight = 10.0

        hc1.ps_agent.setReward(minimizeFeature(hc1.inv_after_allocation), hc1.inv_rwd_weight)
        hc1.ps_agent.setReward(minimizeFeature(hc1.lost_urgent), hc1.lost_urgent_rwd_weight)
        hc1.ps_agent.setReward(minimizeFeature(hc1.backlog), hc1.lost_non_urgent_rwd_weight)

        hc2 = self.agent_converter.get_psychsim_agent(self.hc2)
        hc2.inv_rwd_weight = 1.0
        hc2.lost_urgent_rwd_weight = 100.0
        hc2.lost_non_urgent_rwd_weight = 10.0

        hc2.ps_agent.setReward(minimizeFeature(hc2.inv_after_allocation), hc2.inv_rwd_weight)
        hc2.ps_agent.setReward(minimizeFeature(hc2.lost_urgent), hc2.lost_urgent_rwd_weight)
        hc2.ps_agent.setReward(minimizeFeature(hc2.backlog), hc2.lost_non_urgent_rwd_weight)

    def add_health_centers_recipes(self):
        """
        Creates actions (combinations of recipes) for PsychSim health centers.
        Default is allocate proportional, order up-to, order split by trust, update trust by history.
        """
        for hc in self.simulation.health_centers:
            # creates list of recipes

            available_recipes = {
                ALLOCATION: [HospitalAllocateProportionalRecipe('alloc_prop')],
                # FORECAST_DEMAND: [HospitalDemandForecastRecipe('dem_fore')],
                # CALCULATE_UP_TO_LEVEL: [HospitalUpToLevelRecipe('calc_up')],
                ORDER_AMOUNT: [HospitalUpToOrderAmountRecipe('1ord_up')],
                ORDERING_SPLIT: [HospitalOrderSplitEquallyRecipe('split_eq')],
                TRUST: [HospitalUpdateTrustByHistoryRecipe('upd_trust_hist', 0.8)]
            }

            # creates state-action dynamics
            agent = self.agent_converter.get_psychsim_agent(hc)
            agent.define_state_dynamics(available_recipes)

    def add_distributors_recipes(self):
        """
        Creates actions (combinations of recipes) for PsychSim distributors.
        Default is allocate proportional, order up-to, order split by trust, update trust by history.
        """
        for ds in self.simulation.distributors:
            # creates list of recipes
            available_recipes = {
                ALLOCATION: [DistributorAllocateProportionalRecipe('alloc_prop')],
                # FORECAST_DEMAND: [DistributorDemandForecastRecipe('dem_fore')],
                # CALCULATE_UP_TO_LEVEL: [DistributorUpToLevelRecipe('calc_up')],
                ORDER_AMOUNT: [DistributorUpToOrderAmountRecipe('1ord_up')],
                ORDERING_SPLIT: [DistributorOrderSplitEquallyRecipe('split_eq')],
                # TRUST: [DistributorUpdateTrustByHistoryRecipe('upd_trust_hist', 0.8)]
            }
            # creates state-action dynamics
            agent = self.agent_converter.get_psychsim_agent(ds)
            agent.define_state_dynamics(available_recipes)

    def add_manufacturers_recipes(self):
        """
        Creates actions (combinations of recipes) for PsychSim manufacturers.
        Default is allocate proportional, produce up-to.
        """
        for mn in self.simulation.manufacturers:
            available_recipes = {
                ALLOCATION: [ManufacturerAllocateProportionalRecipe('alloc_prop')],
                # FORECAST_DEMAND: [ManufacturerDemandForecastRecipe('dem_fore')],
                # CALCULATE_UP_TO_LEVEL: [ManufacturerUpToLevelRecipe('calc_up')],
                PRODUCTION_AMOUNT: [ManufacturerUpToProductionRecipe('prod_pro')]
            }

            # creates state-action dynamics
            agent = self.agent_converter.get_psychsim_agent(mn)
            agent.define_state_dynamics(available_recipes)

    def add_cleaning_agent_recipes(self):
        """
        Creates actions (combinations of recipes) for PsychSim cleaning agent.
        Default is simply add the default cleaning actions.
        """
        # creates state-action dynamics
        self.cleaning_agent.define_state_dynamics({})

    def add_mental_models(self):
        """
        Adds mental models for each PsychSim agent.
        Default is add a mental copy of the 'True' model for each agent and sets that model for all other agents.
        """
        for agent in list(self.world.agents.values()):

            # adds a copy of the true model
            agent.addModel(TRUE_MODEL)

            # adds model to all other agents
            for other in list(self.world.agents.values()):
                if other != agent:
                    self.world.setMentalModel(other.name, agent.name, {TRUE_MODEL: 1})

    def add_perceptual_beliefs(self):
        """
        Sets perceptual beliefs for every agent.
        Default is no beliefs, i.e., all agents have perfect perception.
        """
        pass

    def add_action_beliefs(self):
        """
        Sets action beliefs for every agent by setting action legality according to models.
        Default is no beliefs, i.e., all agents have perfect perception.
        """
        pass
