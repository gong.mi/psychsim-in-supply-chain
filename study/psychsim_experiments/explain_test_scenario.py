from flow_simulation.classes.disruption import LineShutDownDisruption
from study.psychsim_experiments.simulation_profile_111 import *

class Explain_test(SimulationProfileBase111):
    """
    A simulation profile for testing having two health-centers with one ordering based on trust and
     one ordering equally, testing if distributors will treat them differently.
    """

    def __init__(self, name, dist_lookahead_idxs):
        """
        Creates and configures the profile.
        :param str name: the name of this profile (used to create results folder).
        :param list dist_lookahead_idxs: a list with the indexes of the distributors with planning capacity.
        """
        self.dist_lookahead_idxs = dist_lookahead_idxs
        super(Explain_test, self).__init__(name)
        self.time_periods = 50

    def parameterize_agent_builder(self):
        """
        Sets the default parameters for the agent builder.
        """
        super(Explain_test, self).parameterize_agent_builder()
        self.agent_builder.history_preserve_time = 40
        self.agent_builder.fixed_order_up_to_level = False

    def add_patient_model(self):
        """
        Adds one patient model to generate demand at the health centers.
        """
        hc1 = self.simulation.health_centers[0]
        patient_model = ConstantPatientModel([hc1])
        patient_model.urgent = 0
        patient_model.non_urgent = 120
        self.simulation.patient_model = patient_model

    def parameterize_psychsim_agents(self):
        super(Explain_test, self).parameterize_psychsim_agents()

        # default parameters for all agents
        for agent in self.agent_converter.all_psychsim_agents():
            agent.ps_agent.setHorizon(12)

    def add_reward_function(self):
    # set the reward function for manufacturers
        mn1 = self.agent_converter.get_psychsim_agent(self.mn1)
        mn1.tot_bklog_after_alloc_rwd_weight = 10.0
        mn1.inv_after_alloc_rwd_weight = 1.0
        mn1.allocate_rwd_weight = 5.0

        mn1.ps_agent.setReward(minimizeFeature(mn1.inv_after_allocation), mn1.inv_after_alloc_rwd_weight)
        mn1.ps_agent.setReward(
            minimizeFeature(mn1.total_backlog_after_allocation), mn1.tot_bklog_after_alloc_rwd_weight)
        for d in mn1.down_stream_agents:
            mn1.ps_agent.setReward(maximizeFeature(mn1.allocate_to[d]), mn1.allocate_rwd_weight)

        # set the reward function of the distributors
        ds1 = self.agent_converter.get_psychsim_agent(self.ds1)
        ds1.tot_bklog_after_alloc_rwd_weight = 10.0
        ds1.inv_after_alloc_rwd_weight = 1.0
        ds1.allocate_rwd_weight = 5.0
        ds1.ps_agent.setReward(minimizeFeature(ds1.inv_after_allocation), ds1.inv_after_alloc_rwd_weight)
        ds1.ps_agent.setReward(
            minimizeFeature(ds1.total_backlog_after_allocation), ds1.tot_bklog_after_alloc_rwd_weight)
        for d in ds1.down_stream_agents:
            ds1.ps_agent.setReward(maximizeFeature(ds1.allocate_to[d]), ds1.allocate_rwd_weight)

        # set reward for health-centers
        hc1 = self.agent_converter.get_psychsim_agent(self.hc1)
        hc1.inv_rwd_weight = 1.0
        hc1.lost_urgent_rwd_weight = 100.0
        hc1.lost_non_urgent_rwd_weight = 10.0

        hc1.ps_agent.setReward(minimizeFeature(hc1.inv_after_allocation), hc1.inv_rwd_weight)
        hc1.ps_agent.setReward(minimizeFeature(hc1.lost_urgent), hc1.lost_urgent_rwd_weight)
        hc1.ps_agent.setReward(minimizeFeature(hc1.backlog), hc1.lost_non_urgent_rwd_weight)

    def add_health_centers_recipes(self):
        """
        Creates actions (combinations of recipes) for PsychSim health centers.
        Default is allocate proportional, order up-to, order split by trust, update trust by history.
        """
        hc1 = self.simulation.health_centers[0]

        available_recipes = {
            ALLOCATION: [HospitalAllocateProportionalRecipe('alloc_prop')],
            # FORECAST_DEMAND: [HospitalDemandForecastRecipe('dem_fore')],
            # CALCULATE_UP_TO_LEVEL: [HospitalUpToLevelRecipe('calc_up')],
            ORDER_AMOUNT: [HospitalUpToOrderAmountRecipe('1ord_up')],
            ORDERING_SPLIT: [HospitalOrderSplitByTrustRecipe('split_trust')],
            TRUST: [HospitalUpdateTrustByHistoryRecipe('upd_trust_hist', 0.55)]
        }

        # creates state-action dynamics
        agent = self.agent_converter.get_psychsim_agent(hc1)
        agent.define_state_dynamics(available_recipes)

    def add_distributors_recipes(self):

        # sets up ordering recipes
        allocate_proportional = DistributorAllocateProportionalRecipe('1alloc_prop')
        allocate_equal = DistributorAllocateEquallyRecipe('alloc_equal')
        # allocate_prefer1 = DistributorAllocateAgentPreferOneRecipe('alloc_prefer_one')
        # allocate_prefer2 = DistributorAllocateAgentPreferTwoRecipe('alloc_prefer_two')

        # allocate_recipes_planning = [allocate_proportional, allocate_prefer1, allocate_prefer2]
        allocate_recipes_planning = [allocate_proportional, allocate_equal]
        allocate_recipes_no_planning = [allocate_proportional]

        # gets distributors with planning capacity
        planning_ds = []
        for idx in self.dist_lookahead_idxs:
            planning_ds.append(self.simulation.distributors[idx])

        # adds corresponding recipes
        for ds in self.simulation.distributors:

            allocation_recipes = allocate_recipes_no_planning
            if ds in planning_ds:
                allocation_recipes = allocate_recipes_planning

            available_recipes = {
                ALLOCATION: allocation_recipes,
                # FORECAST_DEMAND: [DistributorDemandForecastRecipe('dem_fore')],
                # CALCULATE_UP_TO_LEVEL: [DistributorUpToLevelRecipe('calc_up')],
                ORDER_AMOUNT: [DistributorUpToOrderAmountRecipe('1ord_up')],
                ORDERING_SPLIT: [DistributorOrderSplitEquallyRecipe('split_eq')],
                TRUST: [DistributorUpdateTrustByHistoryRecipe('upd_trust_hist', 0.8)]
            }
            # creates state-action dynamics
            agent = self.agent_converter.get_psychsim_agent(ds)
            agent.define_state_dynamics(available_recipes)

    def parameterize_sim_agents(self):
        """
        sets up-to level for 2X2X2 complete network
        """
        # default parametrization
        super(Explain_test, self).parameterize_sim_agents()
        now = self.simulation.now
        for agent in self.simulation.agents:
            agent.up_to_level = agent.order_up_to_level_calculator.calculate(now)

