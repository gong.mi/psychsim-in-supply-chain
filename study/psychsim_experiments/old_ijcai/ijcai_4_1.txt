SCENARIO GOAL:
- If distributors knows that health-centers split order using trustworthiness measure with α=0.2,β=0.8 versus
  α=0.8,β=0.2 and can choose order up-to, order more and order less,
  will it behave differently according to health-centers trust update models?

THIS CODE
- distributor reward = Maximize allocation
- trustworthiness measure with α=0.2,β=0.8

NETWORK STRUCTURE
- nodes:
    2 manufacturers (mn1 and mn2)
    2 distributors (ds1 and ds2)
    2 health centers (hc1 and hc2)
- network connections:
    (mn1, ds1)
    (mn2, ds2)
    (ds1, hc1)
    (ds1, hc2)
    (ds2, hc1)
    (ds2, hc2)

DEMAND PROFILE
- both have 120 non-urgent patients (patient will be backlogged if not satisfied)

ACTIONS
- Health-centers [order up-to, split by trust(α=0.2,β=0.8), allocate proportional]
- Distributors [order up-to (more and less), split equally, allocate proportional]
- Manufacturers [produce up-to, allocate proportional]


DISRUPTION
- mn1 has no disruption
- mn2 has disruption between 20 and 30, its capacity reduces to 85% of the original capacity

PERCEPTION
- all agents have perfect model of each other

PLANNING
- ds1, ds2, or both have forward planning / lookahead capacity
- planning horizon = 6 (12 including cleaning agent)