GOAL:
- having two health-centers one demand of 180, other 60, both ordering based on trust,
 testing if distributor will treat them differently

NETWORK STRUCTURE
- nodes:
    2 manufacturers (mn1 and mn2)
    2 distributors (ds1 and ds2)
    2 health centers (hc1 and hc2)
- network connections:
    (mn1, ds1)
    (mn2, ds2)
    (ds1, hc1)
    (ds1, hc2)
    (ds2, hc1)
    (ds2, hc2)

DEMAND PROFILE
- hc1 has 180 urgent patients (patient will be lost if not satisfied)
- hc2 has 60 urgent patients (patient will be lost if not satisfied)

ACTIONS
- distributors with planning have allocate allocate proportionally, prefer HC1 or prefer HC2
- health-centers split order by trust

DISRUPTION
- mn1 has no disruption
- mn2 has disruption between 20 and 30, its capacity reduces to 85% of the original capacity

PERCEPTION
- all agents have perfect model of each other

PLANNING
- ds1, ds2, or both have forward planning / lookahead capacity