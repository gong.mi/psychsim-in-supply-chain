from psychsim_agents.helper_functions import *
from flow_simulation.classes.disruption import LineShutDownDisruption
from study.psychsim_experiments.simulation_profile_222 import *
from study.psychsim_experiments import *

ALLOCATE_PROPORTIONAL = 'alloc_prop'
ALLOCATE_EQUALLY = 'alloc_eq'
FALSE_MODEL = 'false_model'
TRUE_MODEL = 'true_model'


class ImperfectActionHcDs(SimulationProfileBase222):
    """
    A simulation profile for testing the effect of distributor(s) having a false model of disrupted manufacturer's
    available allocation recipe.
    """

    def __init__(self, name, hc_false_model_idxs, hc_planning_idxs, delta, ds_true_recipe=ALLOCATE_PROPORTIONAL):
        """
        Creates and configures the profile.
        :type delta: float: sensitivity factor
        :param str name: the name of this profile (used to create results folder).
        :param list dist_false_model_idxs: a list with the indexes of the distributors having the false model of the
        manufacturer's allocation recipe.
        :param list dist_planning_idxs: a list with the indexes of the distributors with planning capacity.
        :param str mn_true_recipe: the id of the allocation recipe for the manufacturer (true model).
        """
        self.hc_false_model_idxs = hc_false_model_idxs
        self.hc_planning_idxs = hc_planning_idxs
        self.delta = delta
        self.ds_true_recipe = ds_true_recipe
        super(ImperfectActionHcDs, self).__init__(name)
        self.time_periods = 250

    def parameterize_agent_builder(self):
        """
        Sets the default parameters for the agent builder.
        """
        super(ImperfectActionHcDs, self).parameterize_agent_builder()
        self.agent_builder.history_preserve_time = 40
        self.agent_builder.fixed_order_up_to_level = False


    def add_disruptions(self):
        # manufacturer shutdown with custom period
        manufacturer_shutdown = LineShutDownDisruption(self.simulation, 40)
        manufacturer_shutdown.manufacturer_id = 1
        manufacturer_shutdown.happen_day_1 = 70
        manufacturer_shutdown.end_day_1 = 80
        manufacturer_shutdown.decrease_factor_1 = 0.95
        self.simulation.disruptions.append(manufacturer_shutdown)

    def define_agent_connections(self):
        """
        Collects the references to the agents in the network. Does not change connectivity.
        """
        self.hc1 = self.simulation.health_centers[0]
        self.hc2 = self.simulation.health_centers[1]
        self.ds1 = self.simulation.distributors[0]
        self.ds2 = self.simulation.distributors[1]
        self.mn1 = self.simulation.manufacturers[0]
        self.mn2 = self.simulation.manufacturers[1]

        for ds in self.simulation.distributors:

            # ds <-> hc
            for hc in self.simulation.health_centers:
                hc.upstream_nodes.append(ds)
                ds.downstream_nodes.append(hc)
        self.simulation.distributors[0].upstream_nodes.append(self.simulation.manufacturers[0])
        self.simulation.distributors[1].upstream_nodes.append(self.simulation.manufacturers[1])
        self.simulation.manufacturers[0].downstream_nodes.append(self.simulation.distributors[0])
        self.simulation.manufacturers[1].downstream_nodes.append(self.simulation.distributors[1])

    def add_patient_model(self):
        """
        Adds one patient model to generate demand at the health centers.
        """
        hc1 = self.simulation.health_centers[0]
        hc2 = self.simulation.health_centers[1]
        patient_model = DifferentPatientModel([hc1, hc2])
        patient_model.urgent_1 = 0
        patient_model.non_urgent_1 = 120
        patient_model.urgent_2 = 0
        patient_model.non_urgent_2 = 120
        self.simulation.patient_model = patient_model

    def parameterize_psychsim_agents(self):
        super(ImperfectActionHcDs, self).parameterize_psychsim_agents()

        # is horizon of 6 for allocation enough?
        for agent in self.agent_converter.all_psychsim_agents():
            agent.ps_agent.setHorizon(6)

    def add_reward_function(self):
    # set the reward function for manufacturers
        mn1 = self.agent_converter.get_psychsim_agent(self.mn1)
        mn1.tot_bklog_after_alloc_rwd_weight = 10.0
        mn1.inv_after_alloc_rwd_weight = 1.0
        mn1.allocate_rwd_weight = 5.0

        mn1.ps_agent.setReward(minimizeFeature(mn1.inv_after_allocation), mn1.inv_after_alloc_rwd_weight)
        mn1.ps_agent.setReward(
            minimizeFeature(mn1.total_backlog_after_allocation), mn1.tot_bklog_after_alloc_rwd_weight)
        for d in mn1.down_stream_agents:
            mn1.ps_agent.setReward(maximizeFeature(mn1.allocate_to[d]), mn1.allocate_rwd_weight)

        mn2 = self.agent_converter.get_psychsim_agent(self.mn2)
        mn2.tot_bklog_after_alloc_rwd_weight = 10.0
        mn2.inv_after_alloc_rwd_weight = 1.0
        mn2.allocate_rwd_weight = 5.0

        mn2.ps_agent.setReward(minimizeFeature(mn2.inv_after_allocation), mn2.inv_after_alloc_rwd_weight)
        mn2.ps_agent.setReward(
            minimizeFeature(mn2.total_backlog_after_allocation), mn2.tot_bklog_after_alloc_rwd_weight)
        for d in mn2.down_stream_agents:
            mn2.ps_agent.setReward(maximizeFeature(mn2.allocate_to[d]), mn2.allocate_rwd_weight)

        # set the reward function of the distributors
        ds1 = self.agent_converter.get_psychsim_agent(self.ds1)
        ds1.tot_bklog_after_alloc_rwd_weight = 10.0
        ds1.inv_after_alloc_rwd_weight = 1.0
        ds1.allocate_rwd_weight = 5.0
        ds1.ps_agent.setReward(minimizeFeature(ds1.inv_after_allocation), ds1.inv_after_alloc_rwd_weight)
        ds1.ps_agent.setReward(
            minimizeFeature(ds1.total_backlog_after_allocation), ds1.tot_bklog_after_alloc_rwd_weight)
        for d in ds1.down_stream_agents:
            ds1.ps_agent.setReward(maximizeFeature(ds1.allocate_to[d]), ds1.allocate_rwd_weight)

        # ds2
        ds2 = self.agent_converter.get_psychsim_agent(self.ds2)
        ds2.tot_bklog_after_alloc_rwd_weight = 10.0
        ds2.inv_after_alloc_rwd_weight = 1.0
        ds2.allocate_rwd_weight = 5.0
        ds2.ps_agent.setReward(minimizeFeature(ds2.inv_after_allocation), ds2.inv_after_alloc_rwd_weight)
        ds2.ps_agent.setReward(
            minimizeFeature(ds2.total_backlog_after_allocation), ds2.tot_bklog_after_alloc_rwd_weight)
        for d in ds2.down_stream_agents:
            ds2.ps_agent.setReward(maximizeFeature(ds2.allocate_to[d]), ds2.allocate_rwd_weight)

        # set reward for health-centers
        hc1 = self.agent_converter.get_psychsim_agent(self.hc1)
        hc1.inv_rwd_weight = 1.0
        hc1.lost_urgent_rwd_weight = 100.0
        hc1.lost_non_urgent_rwd_weight = 10.0

        hc1.ps_agent.setReward(minimizeFeature(hc1.inv_after_allocation), hc1.inv_rwd_weight)
        hc1.ps_agent.setReward(minimizeFeature(hc1.lost_urgent), hc1.lost_urgent_rwd_weight)
        hc1.ps_agent.setReward(minimizeFeature(hc1.backlog), hc1.lost_non_urgent_rwd_weight)

        hc2 = self.agent_converter.get_psychsim_agent(self.hc2)
        hc2.inv_rwd_weight = 1.0
        hc2.lost_urgent_rwd_weight = 100.0
        hc2.lost_non_urgent_rwd_weight = 10.0

        hc2.ps_agent.setReward(minimizeFeature(hc2.inv_after_allocation), hc2.inv_rwd_weight)
        hc2.ps_agent.setReward(minimizeFeature(hc2.lost_urgent), hc2.lost_urgent_rwd_weight)
        hc2.ps_agent.setReward(minimizeFeature(hc2.backlog), hc2.lost_non_urgent_rwd_weight)

    def add_health_centers_recipes(self):
        """
        Creates actions (combinations of recipes) for PsychSim health centers.
        """
        order_offset = 0.2
        order_up_to = HospitalUpToOrderAmountRecipe('1ord_up')
        order_more = HospitalUpToOrderAmountRecipe('ord+', order_offset)
        order_less = HospitalUpToOrderAmountRecipe('ord-', -order_offset)
        order_amt_recipes_planning = [order_up_to, order_less, order_more]
        order_amt_recipes_no_planning = [order_up_to]

        # gets health centers with planning capacity
        planning_hc = []
        for idx in self.hc_planning_idxs:
            planning_hc.append(self.simulation.health_centers[idx])

        # adds recipes
        for hc in self.simulation.health_centers:
            order_amt_recipes = order_amt_recipes_no_planning
            if hc in planning_hc:
                order_amt_recipes = order_amt_recipes_planning

            available_recipes = {
                ALLOCATION: [HospitalAllocateProportionalRecipe('alloc_prop')],
                # FORECAST_DEMAND: [DistributorDemandForecastRecipe('dem_fore')],
                # CALCULATE_UP_TO_LEVEL: [DistributorUpToLevelRecipe('calc_up')],
                ORDER_AMOUNT: order_amt_recipes,
                ORDERING_SPLIT: [HospitalOrderSplitEquallyRecipe('split_eq')],
                TRUST: [HospitalUpdateTrustByHistoryRecipe('upd_trust_hist', self.delta)]
            }
            # creates state-action dynamics
            agent = self.agent_converter.get_psychsim_agent(hc)
            agent.define_state_dynamics(available_recipes)

    def add_distributors_recipes(self):
        # sets up allocation recipes
        if self.ds_true_recipe == ALLOCATE_PROPORTIONAL:
            true_allocate = DistributorAllocateProportionalRecipe(ALLOCATE_PROPORTIONAL)
            false_allocate = DistributorAllocateEquallyRecipe(ALLOCATE_EQUALLY)
        else:
            true_allocate = DistributorAllocateEquallyRecipe(ALLOCATE_EQUALLY)
            false_allocate = DistributorAllocateProportionalRecipe(ALLOCATE_PROPORTIONAL)

        for ds in self.simulation.distributors:

            # only DS2 will have two recipes
            if ds == self.ds2:
                allocation_recipes = [true_allocate, false_allocate]
            else:
                allocation_recipes = [DistributorAllocateProportionalRecipe(ALLOCATE_PROPORTIONAL)]

            available_recipes = {
                ALLOCATION: allocation_recipes,
                # FORECAST_DEMAND: [ManufacturerDemandForecastRecipe('dem_fore')],
                ORDER_AMOUNT: [DistributorUpToOrderAmountRecipe('1ord_up')],
                ORDERING_SPLIT: [DistributorOrderSplitEquallyRecipe('split_eq')],
                # TRUST: [DistributorUpdateTrustByHistoryRecipe('upd_trust_hist', 0.8)]
            }

            # creates state-action dynamics
            agent = self.agent_converter.get_psychsim_agent(ds)
            agent.define_state_dynamics(available_recipes)


    def add_mental_models(self):

        # gets healthcenter with false model of DS2
        false_model_hc = []
        for idx in self.hc_false_model_idxs:
            false_model_hc.append(self.agent_converter.get_psychsim_agent(self.simulation.health_centers[idx]).ps_agent)

        # adds false model only to DS2
        ds2 = self.agent_converter.get_psychsim_agent(self.ds2).ps_agent
        ds2.addModel(FALSE_MODEL)

        for agent in list(self.world.agents.values()):
            # adds a copy of the true model for all agents
            agent.addModel(TRUE_MODEL)

            # adds respective model to all other agents
            for other in list(self.world.agents.values()):
                if other != agent:
                    # a HC has false model of DS2
                    if agent == ds2 and other in false_model_hc:
                        self.world.setMentalModel(other.name, agent.name, {FALSE_MODEL: 1})
                    else:
                        self.world.setMentalModel(other.name, agent.name, {TRUE_MODEL: 1})

        for hc in false_model_hc:
            # tells HC2's model (used by others) to ignore the parent's beliefs (imperfections)
            hc.setAttribute('ignore-parent-beliefs', True, TRUE_MODEL)

    def add_action_beliefs(self):
        # sets illegal actions for DS2 models according to allocation recipe
        ds2 = self.agent_converter.get_psychsim_agent(self.ds2).ps_agent
        for action in ds2.actions:
            if self.ds_true_recipe in action['verb']:
                set_illegal_action(ds2, action, [True, TRUE_MODEL])
            else:
                set_illegal_action(ds2, action, [FALSE_MODEL])

    def parameterize_sim_agents(self):
        """
        sets up-to level for 2X2X2 complete network
        """
        # default parametrization
        super(ImperfectActionHcDs, self).parameterize_sim_agents()
        now = self.simulation.now
        for agent in self.simulation.agents:
            agent.up_to_level = agent.order_up_to_level_calculator.calculate(now)