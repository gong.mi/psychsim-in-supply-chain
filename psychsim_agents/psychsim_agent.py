from psychsim.agent import Agent
from psychsim.world import World
from .agent_converter import AgentConverter


class PsychSimAgent(object):
    """
    PsychSimAgent is a base class for all the PsychSim agents.
    It creates a connection between the simulator and PsychSim agents.
    """

    def __init__(self, world, agent_converter, name="Agent"):
        """
        Creates a PsychSim agent with the given name
        :param World world: the PsychSim world to which attach the agent.
        :param AgentConverter agent_converter: the correspondence list between PsychSim agent ids and simulation agents.
        :param str name: the name of the agent to be created in PsychSim.
        """
        self.world = world
        self.agent_converter = agent_converter
        self.name = name

        # creates PsychSim agent
        self.ps_agent = Agent(name)

        # note: by adding ps_agent to self.world, the world of ps_agent will also be self.world
        self.world.addAgent(self.ps_agent)

        # common parameters
        self.smoothing_constant = 0.8
        self.totals = {}

    def create_state_variables(self):
        """
        Each agent has to create its own state variables.
        """
        pass

    def define_dependencies(self):
        """
        Each agent has to define the dependencies between the state variables.
        """
        pass

    def define_state_dynamics(self, available_recipes):
        """
        Each agent has to define the state variable dynamics according to its own recipes.
        :param dict available_recipes: a dictionary containing the different types of recipes available for the agent.
        """
        pass

    def update_psychsim_from_simulation(self, now):
        """
        Updates the agent's PsychSim state according to the simulation agent state.
        """
        pass

    def update_simulation_from_psychsim(self, outcomes):
        """
        Updates the simulation agent decisions according to the PsychSim agent's actions.
        """
        pass

    def __str__(self):
        return self.ps_agent.name

    def __hash__(self):
        return self.ps_agent.name.__hash__()

    @staticmethod
    def get_all_recipe_combinations(available_recipes):
        """
        Utility method that creates all possible combinations between the given recipes.
        :param dict available_recipes: a dictionary containing pairs of recipe type (str) - list of available recipes.
        :return: a list containing dictionaries of pairs recipe type (str) - recipe, each corresponding to a different
        combination of available recipes.
        :rtype list
        """
        all_recipe_combs = [{}]
        for key, recipes in available_recipes.items():
            new_recipe_combs = []
            for recipe_comb in all_recipe_combs:
                for recipe in recipes:
                    new_recipe_comb = dict(recipe_comb)
                    new_recipe_comb[key] = recipe
                    new_recipe_combs.append(new_recipe_comb)
            all_recipe_combs = new_recipe_combs
        return all_recipe_combs
