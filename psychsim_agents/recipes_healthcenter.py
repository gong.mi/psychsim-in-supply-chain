""" All trust, allocation and ordering recipes for psychsim healthcenter """
from .recipe import Recipe
from .helper_functions import *
from .constants import *
from psychsim.action import Action


class HospitalReceiveShipmentRecipe(Recipe):
    """Utility recipe for updating received inventory based on received dock"""

    def register_recipe(self, agent, action):
        """
        Registers the recipe in the action.
        :param psychsim_agents.psychsim_health_center.PSHealthCenterAgent agent: the agent for which to register the recipe.
        :param Action action: the action for which the recipe is going to be added.
        """
        # update total inventory
        update_inv_dict = {agent.total_inventory: 1.0}
        for u in agent.up_stream_agents:
            update_inv_dict[agent.received_dock[u]] = 1.0
        tree = makeTree(multiSetMatrix(agent.inv_after_received, update_inv_dict))
        agent.world.setDynamics(agent.inv_after_received, action, tree)

        # deduct from on-order of upstream
        for u in agent.up_stream_agents:
            tree = makeTree(
                multiSetMatrix(agent.onorder_after_received[u],
                               {agent.on_order_dict[u]: 1.0, agent.received_dock[u]: -1.0}))
            agent.world.setDynamics(agent.onorder_after_received[u], action, tree)


class HospitalUpdateTrustByHistoryRecipe(Recipe):
    """Updates the trust that an agent is associated to its upstream nodes based on placed order and received order"""

    def __init__(self, name="Recipe", delta=0.5):
        super(HospitalUpdateTrustByHistoryRecipe, self).__init__(name)
        self.delta = delta

    def register_recipe(self, agent, action):
        """
        Registers the recipe in the action.
        :param psychsim_agents.psychsim_health_center.PSHealthCenterAgent agent: the agent for which to register the recipe.
        :param Action action: the action for which the recipe is going to be added.
        """
        for s in agent.up_stream_agents:
            tree = makeTree(multiSetMatrix(agent.trust[s],
                                           {agent.trust[s]: 1 - self.delta, agent.ontime_deliv[s]: self.delta}))
            agent.world.setDynamics(agent.trust[s], action, tree)


# class HospitalAsymmetricUpdateTrustRecipe(Recipe):
#     """Updates the trust that an agent is associated to its upstream nodes based on placed order and received order"""
#
#     def __init__(self, name="Recipe", alpha=0.2, beta=0.8):
#         super(HospitalAsymmetricUpdateTrustRecipe, self).__init__(name)
#         # alpha/beta is skepticism level of the agent
#         self.alpha = alpha
#         self.beta = beta
#
#     def register_recipe(self, agent, action):
#         """
#         Registers the recipe in the action.
#         :param psychsim_agents.psychsim_health_center.PSHealthCenterAgent agent: the agent for which to register the recipe.
#         :param Action action: the action for which the recipe is going to be added.
#         """
#         for s in agent.up_stream_agents:
#             tree = makeTree({'if': multiCompareRow({agent.ontime_deliv[s]: 1.0, agent.trust[s]: -1.0}),
#                              True: multiSetMatrix(agent.trust[s],
#                                                   {agent.trust[s]: self.alpha, agent.ontime_deliv[s]: 1 - self.alpha}),
#                              False: multiSetMatrix(agent.trust[s],
#                                                    {agent.trust[s]: self.beta, agent.ontime_deliv[s]: 1 - self.beta})})
#             agent.world.setDynamics(agent.trust[s], action, tree)


# class HospitalAllocateUrgentRecipe(Recipe):
#     def register_recipe(self, agent, action):
#         """
#         Registers the recipe in the action.
#         :param psychsim_agents.psychsim_health_center.PSHealthCenterAgent agent: the agent for which to register the recipe.
#         :param Action action: the action for which the recipe is going to be added.
#         """
#         # total demand is summation of current urgent and non-urgent plus the backlog
#         tree = makeTree(multiSetMatrix(agent.total_demand, {agent.urgent_cases: 1.0, agent.non_urgent_cases: 1.0,
#                                                             agent.backlog: 1.0}))
#         agent.world.setDynamics(agent.total_demand, action, tree)
#
#         # allocation (treatment) dynamics
#         tree = makeTree(
#             {'if': multiCompareRow(
#                 {agent.inv_after_received: 1.0, agent.total_demand: -1.0, agent.treat_threshold: -1.0}),
#                 True: setToFeatureMatrix(agent.urgent_satisfied, agent.urgent_cases),
#                 False: {'if': greaterThanRow(agent.inv_after_received, agent.urgent_cases),
#                         True: setToFeatureMatrix(agent.urgent_satisfied, agent.urgent_cases),
#                         False: {'if': thresholdRow(agent.inv_after_received, 0),
#                                 True: setToFeatureMatrix(agent.urgent_satisfied, agent.inv_after_received),
#                                 False: setToConstantMatrix(agent.urgent_satisfied, 0)}}})
#         agent.world.setDynamics(agent.urgent_satisfied, action, tree)
#         tree = makeTree(
#             {'if': multiCompareRow(
#                 {agent.inv_after_received: 1.0, agent.total_demand: -1.0, agent.treat_threshold: -1.0}),
#                 True: multiSetMatrix(agent.non_urgent_satisfied, {agent.non_urgent_cases: 1.0,
#                                                                   agent.backlog: 1.0}),
#                 False: {'if': multiCompareRow(
#                     {agent.inv_after_received: 1.0, agent.urgent_cases: -1.0, agent.treat_threshold: -1.0}),
#                     True: multiSetMatrix(agent.non_urgent_satisfied,
#                                          {agent.inv_after_received: 1.0, agent.urgent_cases: -1.0,
#                                           agent.treat_threshold: -1.0}),
#                     False: setToConstantMatrix(agent.non_urgent_satisfied, 0)}})
#         agent.world.setDynamics(agent.non_urgent_satisfied, action, tree)


class HospitalAllocateProportionalRecipe(Recipe):
    def register_recipe(self, agent, action):
        """
        Registers the recipe in the action. Uses proportional i.e.,  U = (U * Inv)/(N+U)
        :param psychsim_agents.psychsim_health_center.PSHealthCenterAgent agent: the agent for which to register the recipe.
        :param Action action: the action for which the recipe is going to be added.
        """

        # total demand is summation of current urgent and non-urgent plus the backlog
        tree = makeTree(multiSetMatrix(agent.total_demand, {agent.urgent_cases: 1.0, agent.non_urgent_cases: 1.0,
                                                            agent.backlog: 1.0}))
        agent.world.setDynamics(agent.total_demand, action, tree)

        # urgent
        tree = makeTree({'if': differenceRow(agent.inv_after_received, agent.total_demand, 0),
                         True: setToFeatureMatrix(agent.temp_urgent, agent.urgent_cases),
                         False: {'if': greaterThanRow(agent.inv_after_received, 0),
                                 True: setToScaledKeyMatrix(agent.temp_urgent, agent.inv_after_received,
                                                            agent.urgent_cases, agent.total_demand, .01),
                                 False: setToConstantMatrix(agent.temp_urgent, 0)}})
        agent.world.setDynamics(agent.temp_urgent, action, tree)
        # make sure allocation amount is not more than demand
        tree = makeTree({'if': greaterThanRow(agent.temp_urgent, agent.urgent_cases),
                         True: setToFeatureMatrix(agent.urgent_satisfied, agent.urgent_cases),
                         False: setToFeatureMatrix(agent.urgent_satisfied, agent.temp_urgent)})
        agent.world.setDynamics(agent.urgent_satisfied, action, tree)

        # non-urgent
        tree = makeTree({'if': differenceRow(agent.inv_after_received, agent.total_demand, 0),
                         True: multiSetMatrix(agent.temp_non_urgent, {agent.non_urgent_cases: 1.0,
                                                                      agent.backlog: 1.0}),
                         False: {'if': greaterThanRow(agent.inv_after_received, 0),
                                 True: multiSetMatrix(agent.temp_non_urgent, {agent.inv_after_received: 1.0,
                                                                              agent.urgent_satisfied: -1.0}),
                                 False: setToConstantMatrix(agent.temp_non_urgent, 0)}})
        agent.world.setDynamics(agent.temp_non_urgent, action, tree)

        # make sure allocation amount is not more than demand
        tree = makeTree({'if': multiCompareRow({agent.temp_non_urgent: 1.0,
                                                agent.non_urgent_cases: -1.0,
                                                agent.backlog: -1.0}),
                         True: multiSetMatrix(agent.non_urgent_satisfied, {agent.non_urgent_cases: 1.0,
                                                                           agent.backlog: 1.0}),
                         False: setToFeatureMatrix(agent.non_urgent_satisfied, agent.temp_non_urgent)})
        agent.world.setDynamics(agent.non_urgent_satisfied, action, tree)


class HospitalIntermediateStateUpdateRecipe(Recipe):
    """Utility recipe to aggregate ordering and allocation policies in Hospital agents"""

    def register_recipe(self, agent, action):
        """
        Registers the recipe in the action.
        :param psychsim_agents.psychsim_health_center.PSHealthCenterAgent agent: the agent for which to register the recipe.
        :param Action action: the action for which the recipe is going to be added.
        """

        # dummy variable for forcing PsychSim to use updated inventory when ordering
        tree = makeTree(multiSetMatrix(agent.inv_after_allocation,
                                       {agent.inv_after_received: 1.0, agent.non_urgent_satisfied: -1.0,
                                        agent.urgent_satisfied: -1.0}))
        agent.world.setDynamics(agent.inv_after_allocation, action, tree)

        # update backlog after allocation
        tree = makeTree(multiSetMatrix(agent.backlog_after_allocation, {agent.backlog: 1.0, agent.non_urgent_cases: 1.0,
                                                                        agent.non_urgent_satisfied: -1.0}))
        agent.world.setDynamics(agent.backlog_after_allocation, action, tree)


class HospitalDemandForecastRecipe(Recipe):
    def register_recipe(self, agent, action):
        """
        Registers the recipe in the action.
        :param psychsim_agents.psychsim_health_center.PSHealthCenterAgent agent: the agent for which to register the recipe.
        :param Action action: the action for which the recipe is going to be added.
        """

        pass


class HospitalUpToLevelRecipe(Recipe):
    def register_recipe(self, agent, action):
        pass


class HospitalUpToOrderAmountRecipe(Recipe):
    def __init__(self, name="Recipe", offset=0.0):
        super(HospitalUpToOrderAmountRecipe, self).__init__(name)
        self.offset = offset

    def register_recipe(self, agent, action):
        """
        Registers the recipe in the action.
        :param psychsim_agents.psychsim_health_center.PSHealthCenterAgent agent: the agent for which to register the recipe.
        :param Action action: the action for which the recipe is going to be added.
        """
        # total on-order
        update_onorder_dict = {}
        for s in agent.up_stream_agents:
            update_onorder_dict[agent.onorder_after_received[s]] = 1.0
        tree = makeTree(multiSetMatrix(agent.total_on_order, update_onorder_dict))
        agent.world.setDynamics(agent.total_on_order, action, tree)
        # splits order total offset by trust
        offset_factor = 1.0 + self.offset
        tree = makeTree(
            {'if': multiCompareRow({agent.up_to_level: offset_factor,
                                    agent.backlog_after_allocation: offset_factor,
                                    agent.inv_after_allocation: -offset_factor,
                                    agent.total_on_order: -offset_factor}),
             False: setToConstantMatrix(agent.order_amount, 0),
             True: multiSetMatrix(agent.order_amount,
                                  {agent.up_to_level: offset_factor,
                                   agent.backlog_after_allocation: offset_factor,
                                   agent.inv_after_allocation: -offset_factor,
                                   agent.total_on_order: -offset_factor})})
        agent.world.setDynamics(agent.order_amount, action, tree)


class HospitalOrderSplitByTrustRecipe(Recipe):
    def register_recipe(self, agent, action):
        """
        Registers the recipe in the action.
        :param psychsim_agents.psychsim_health_center.PSHealthCenterAgent agent: the agent for which to register the recipe.
        :param Action action: the action for which the recipe is going to be added.
        """

        # samples_1 = get_univariate_samples(square, 0, 1, 100)
        # for s in agent.up_stream_agents:
        #     tree = makeTree(tree_from_univariate_samples(agent.coef_2[s], agent.trust[s],
        #                                                  samples_1, 0, 99))
        #     agent.world.setDynamics(agent.coef_2[s], action, tree)
        # for s in agent.up_stream_agents:
        #     tree = makeTree(setToFeatureMatrix(agent.coef_2[s], agent.trust[s]))
        #     agent.world.setDynamics(agent.coef_2[s], action, tree)

        coef_dict = {}
        for s in agent.up_stream_agents:
            coef_dict[agent.trust[s]] = 1.0
        tree = makeTree(multiSetMatrix(agent.sum_coef, coef_dict))
        agent.world.setDynamics(agent.sum_coef, action, tree)

        samples_2 = get_bivariate_samples(div, 0, 1, 0, 2, 100, 200)
        for s in agent.up_stream_agents:
            tree = makeTree(tree_from_bivariate_samples(agent.final_coef[s],
                                                        agent.trust[s], agent.sum_coef,
                                                        samples_2, 0, 99, 0, 199))
            agent.world.setDynamics(agent.final_coef[s], action, tree)

        max_o = agent.totals[ORDER_HI]
        num_samples = max_o
        samples_3 = get_bivariate_samples(mult, 0, max_o, 0, 1, num_samples, 100)
        for s in agent.up_stream_agents:
            tree = makeTree(tree_from_bivariate_samples(agent.order_to[s],
                                                        agent.order_amount, agent.final_coef[s],
                                                        samples_3, 0, num_samples - 1, 0, 99))

            agent.world.setDynamics(agent.order_to[s], action, tree)


class HospitalOrderSplitEquallyRecipe(Recipe):
    def register_recipe(self, agent, action):
        """
        Registers the recipe in the action.
        :param psychsim_agents.psychsim_health_center.PSHealthCenterAgent agent: the agent for which to register the recipe.
        :param Action action: the action for which the recipe is going to be added.
        """

        # samples = get_bivariate_samples(division_func, 0, agent.totals[ORDER_HI], 0, agent.totals[UP_STR_HI],
        #                                 agent.totals[ORDER_HI]/10, agent.totals[UP_STR_HI])
        num_upstream = len(agent.up_stream_agents)
        for s in agent.up_stream_agents:
            tree = makeTree(multiSetMatrix(agent.order_to[s], {agent.order_amount: 1.0/num_upstream}))
            agent.world.setDynamics(agent.order_to[s], action, tree)

        # for s in agent.up_stream_agents:
        #     tree = makeTree({'if': thresholdRow(agent.order_amount, 0),
        #                      True: setToScaledKeyMatrix(agent.order_to[s], agent.order_amount, 1.0, agent.num_up_stream,
        #                                                 .01),
        #                      False: setToConstantMatrix(agent.order_to[s], 0)})
        #     # tree = makeTree({'if': thresholdRow(agent.order_amount, 0),
        #     #                  True: tree_from_bivariate_samples(agent.order_to[s], agent.order_amount,
        #     #                                                           agent.num_up_stream, samples, 0, agent.totals[ORDER_HI]/10 - 1, 0, agent.totals[UP_STR_HI]-1),
        #     #                  False: setToConstantMatrix(agent.order_to[s], 0)})
        #     agent.world.setDynamics(agent.order_to[s], action, tree)


class HospitalShortageCalculationRecipe(Recipe):
    def register_recipe(self, agent, action):
        """
        Registers the recipe in the action.
        :param psychsim_agents.psychsim_health_center.PSHealthCenterAgent agent: the agent for which to register the recipe.
        :param Action action: the action for which the recipe is going to be added.
        """
        tree = makeTree({'if': multiCompareRow(
            {agent.urgent_cases: 1.0, agent.urgent_satisfied: -1.0}),
            False: setToConstantMatrix(agent.lost_urgent, 0),
            True: multiSetMatrix(agent.lost_urgent,
                                 {agent.urgent_cases: 1.0, agent.urgent_satisfied: -1.0})})
        agent.world.setDynamics(agent.lost_urgent, action, tree)
