"""
    PsychSim helper functions for dynamics
"""

from psychsim.reward import *

# setToScaledMatrix(Val_to_set,Val_to_scale,numerator,denominator,knot_size,1) the fourth argument sets knot spacing
def setToScaledKeyMatrix(SetVar, V, numerator, denominator, inc,
                         i=1.0):  # simple switch function to handle whether numerator is staekey or a constant
    if (isinstance(numerator, int) or isinstance(numerator, float)):
        return setToScaledDenKeyMatrix(SetVar, V, numerator + inc / 2, denominator, inc,
                                       i=1.0)  # STACY ADDED THRESHOLD
    else:
        return setToScaledKeysMatrix(SetVar, V, numerator, denominator, inc, i, inc / 2)  # STACY ADDED THRESHOLD


### This assumes numerator is a python number and denominator is a statekey
def setToScaledDenKeyMatrix(SetVar, V, numerator, denominator, inc,
                            i):  # dummbest way (would want line segments or at least a balanced tree)
    if (i < inc):
        return setToConstantMatrix(SetVar, 0)  # supply insufficient so set N to 0
    else:
        return {'if': multiCompareRow({denominator: i}, threshold=numerator),
                # are scaled cases greater than supply
                True: setToScaledDenKeyMatrix(SetVar, V, numerator, denominator, inc, i - inc),
                # recurse with a reduced proportion
                False: setToFeatureMatrix(SetVar, V, i)}  # Scale Nonurgent by i


### This assumes both numerator and denominator are statekeys
def setToScaledKeysMatrix(SetVar, V, numerator, denominator, inc, i=1.0,
                          threshold=0):  # dummbest way (would want line segments or at least a balanced tree)
    if (i < inc):
        return setToConstantMatrix(SetVar, 0)  # supply insufficient so set N to 0
    else:
        return {'if': multiCompareRow({denominator: i, numerator: -1.0}, threshold),
                # are scaled cases greater than supply
                True: setToScaledKeysMatrix(SetVar, V, numerator, denominator, inc, i - inc, threshold),
                # recurse with a reduced proportion
                False: setToFeatureMatrix(SetVar, V, i)}  # Scale Nonurgent by i


def setToMinMatrix(Key, Ckey1, Ckey2):
    return {'if': differenceRow(Ckey1, Ckey2, 0.0),
            True: setToFeatureMatrix(Key, Ckey2),
            False: setToFeatureMatrix(Key, Ckey1)}


def setToMaxMatrix(Key, Ckey1, Ckey2):
    return {'if': differenceRow(Ckey1, Ckey2, 0.0),
            True: setToFeatureMatrix(Key, Ckey1),
            False: setToFeatureMatrix(Key, Ckey2)}


# this function just illustrates the guts of conditional tests
def multiCompareRow(ScaledKeys, threshold=0.0):  # scales and sums all the keys and
    # print "scaledcompare", ScaledKeys
    return KeyedPlane(KeyedVector(ScaledKeys), threshold)  # then tests if > threshold


def multiSetMatrix(Key,
                   ScaledKeys):  # scales and sums all the keys in ScaledKeys dict and adds offset if CONSTANT in ScaledKeys
    # print ScaledKeys
    return KeyedMatrix({Key: KeyedVector(
        ScaledKeys)})  # then sets Key which may be in ScaledKeys in which case it is adding to scaled value of Key
