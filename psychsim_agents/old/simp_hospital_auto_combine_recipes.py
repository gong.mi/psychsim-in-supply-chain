"""
    Automatic combination of ordering and allocation recipes
"""

from configparser import SafeConfigParser

from psychsim.agent import Agent
from psychsim.world import World, stateKey, binaryKey

from helper_functions import *


class SimpHospitalAutoCombineRecipes(object):
    def __init__(self, turn_order):
        self.maxRounds = 4
        self.world = World()
        self.numHospitals = 2
        self.treat_threshold = 1
        self.hospital1 = Agent('H1')
        self.hospital2 = Agent('H2')
        self.hosp_agents = [self.hospital1, self.hospital2]
        self.suppAgtNames = {'H1': ['S1', 'S2'], 'H2': ['S1', 'S2']}
        self.totals = {'hInv': 30, 'hUpTo': 30, 'hOnOrder': 30, 'hUpStr': 2, 'hOrderAmt': 50, 'hDem': 30}

        # creates list of allocation and ordering recipes
        self.allocation_recipes = {'allocate_proportional': self.add_allocate_proportional_dynamics,
                                   'allocate_urgent': self.add_allocate_urgent_dynamics}

        self.ordering_recipes = {'order_by_trust': self.add_order_by_trust_dynamics,
                                 'order_equally': self.add_order_equally_dynamics}

        # define Hospital agents: state, actions and parameters common to all Hospital Agents
        for h in self.hosp_agents:
            self.world.addAgent(h)

            # define the State
            self.add_allocation_state_vars(h)
            self.add_ordering_state_vars(h)

            # define dependencies: tell PsychSim the order in which to compute variables
            self.add_ordering_dependencies(h)
            self.add_allocation_dependencies(h)

            # define the actions and dynamics
            self.add_actions(h)

            # define trust network
            for supplier in self.suppAgtNames[h.name]:
                key = self.world.defineRelation(h.name, supplier, 'trust', float, lo=0.0, hi=1.0)
                self.world.setFeature(key, 1.0)

            # set parameters
            h.setHorizon(1)  # Horizon is how far aead agent reasons to determine next action
            h.setAttribute('discount', 0.9)  # Discount is how much agent discounts future rewards

        self.world.setOrder(turn_order)

        # setting trust values
        self.world.setFeature(binaryKey('H1', 'S1', 'trust'), .7)
        self.world.setFeature(binaryKey('H2', 'S1', 'trust'), .7)

        # define reward functions
        self.set_reward_functions()

    def add_actions(self, h):

        # creates combinations of allocation-ordering
        for allocation_recipe in self.allocation_recipes:
            for ordering_recipe in self.ordering_recipes:
                action_name = allocation_recipe + '_' + ordering_recipe

                # create and add atom / action
                h.addAction({'verb': 'decide', 'recipe': action_name})
                atom = Action({'subject': h.name, 'verb': 'decide', 'recipe': action_name})

                # allocation (treatment) dynamics
                self.allocation_recipes[allocation_recipe](atom, h)

                # intermediate update of total inventory using dummy variable
                self.add_inventory_update_dynamics(atom, h)

                # ordering dynamics
                self.ordering_recipes[ordering_recipe](atom, h)

    def add_inventory_update_dynamics(self, atom, h):
        # dummy variable for forcing PsychSim to use updated inventory when ordering
        tree = makeTree(
            multiSetMatrix(h.res_inventory,
                           {h.total_inventory: 1.0, h.non_urgent_satisfied: -1.0, h.urgent_satisfied: -1.0}))
        self.world.setDynamics(h.res_inventory, atom, tree)

        # updates total inventory based on dummy res_inventory
        tree = makeTree(setToFeatureMatrix(h.total_inventory, h.res_inventory))
        self.world.setDynamics(h.total_inventory, atom, tree)

    def add_order_by_trust_dynamics(self, atom, h):
        # splits order total offset by trust
        tree = makeTree({'if': multiCompareRow({h.up_to_level: 1.0, h.res_inventory: -1.0, h.total_on_order: -1.0}),
                         False: setToConstantMatrix(h.order_amount, 0),
                         True: multiSetMatrix(h.order_amount,
                                              {h.up_to_level: 1.0, h.res_inventory: -1.0, h.total_on_order: -1.0})})
        self.world.setDynamics(h.order_amount, atom, tree)
        # sum up the suppliers using their trust value as a kind of weird way to estimate likelihood of delivery
        # note unlike above recipe of split equall this handles the case of suppliers not being active
        supp_dict = {}
        for s in self.suppAgtNames[h.name]:
            supp_dict[binaryKey(h.name, s, "trust")] = 1.0
        tree = makeTree(multiSetMatrix(h.adj_num_up_stream, supp_dict))
        self.world.setDynamics(h.adj_num_up_stream, atom, tree)
        # so now order but the divisor is the summed trust values (so you end up ordering more than OrderAmount
        # and entirely untrusted suppliers get no order at all
        for i in range(0, len(self.suppAgtNames[
                                  h.name])):  # this really should loop over supplier names and do so regardless if they are active
            s = self.suppAgtNames[h.name][i]
            print(s)
            tree = makeTree({'if': thresholdRow(h.order_amount, 0),
                             True: {'if': thresholdRow(binaryKey(h.name, s, "trust"), 0.01),
                                    # is this supplier active/trusted at all
                                    True: setToScaledKeyMatrix(h.order_to[i], h.order_amount, 1.0, h.adj_num_up_stream,
                                                               .1),
                                    False: setToConstantMatrix(h.order_to[i], 0)},
                             False: setToConstantMatrix(h.order_to[i], 0)})
            # instead of Order{i} we probably should directly manipulate beliefs of Supplier (see comment above)
            self.world.setDynamics(h.order_to[i], atom, tree)

    def add_order_equally_dynamics(self, atom, h):
        # splits order equally
        tree = makeTree({'if': multiCompareRow({h.up_to_level: 1.0, h.res_inventory: -1.0, h.total_on_order: -1.0}),
                         False: setToConstantMatrix(h.order_amount, 0),
                         True: multiSetMatrix(h.order_amount,
                                              {h.up_to_level: 1.0, h.res_inventory: -1.0, h.total_on_order: -1.0})})
        self.world.setDynamics(h.order_amount, atom, tree)

        for i in range(0, len(self.suppAgtNames[h.name])):
            tree = makeTree({'if': thresholdRow(h.order_amount, 0),
                             True: setToScaledKeyMatrix(h.order_to[i], h.order_amount, 1.0, h.num_up_stream, .1),
                             False: setToConstantMatrix(h.order_to[i], 0)})
            self.world.setDynamics(h.order_to[i], atom, tree)

    def add_allocate_proportional_dynamics(self, atom, h):
        # uses proportional e.g.,  U = U * ((Inv-T)/(N+U) SO CREATE FOLOWING INTERMEDIATE StateKey VALUES
        # for the denominator and numerator e.g., TotalDemand = N+U and Supppy = Inv-T
        # ScaledUrgent for  U * ((Inv-T)/(N+U)
        tree = makeTree(KeyedMatrix({h.total_demand: KeyedVector({h.urgent_cases: 1.0, h.non_urgent_cases: 1.0})}))
        self.world.setDynamics(h.total_demand, atom, tree)
        tree = makeTree(setToFeatureMatrix(h.supply, h.total_inventory, 1.0, -1.0 * self.treat_threshold))
        self.world.setDynamics(h.supply, atom, tree)
        tree = makeTree(setToScaledKeyMatrix(h.scaled_urgent, h.urgent_cases, h.supply, h.total_demand, .1))
        self.world.setDynamics(h.scaled_urgent, atom, tree)
        # Nonurgent cases
        tree = makeTree({'if': multiCompareRow({h.non_urgent_cases: 1, h.urgent_cases: 1, h.supply: -1.0}, 0.0),
                         False: setToFeatureMatrix(h.non_urgent_satisfied, h.non_urgent_cases),
                         True: {'if': thresholdRow(h.supply, 0),
                                True: setToScaledKeyMatrix(h.non_urgent_satisfied, h.non_urgent_cases, h.supply,
                                                           h.total_demand,
                                                           .1),
                                False: setToConstantMatrix(h.non_urgent_satisfied, 0)}})
        self.world.setDynamics(h.non_urgent_satisfied, atom, tree)
        # Urgent cases
        tree = makeTree({'if': multiCompareRow({h.non_urgent_cases: 1, h.urgent_cases: 1, h.supply: -1}, 0.0),
                         False: setToFeatureMatrix(h.urgent_satisfied, h.urgent_cases),
                         True: {'if': thresholdRow(h.supply, 0),
                                True: {'if': multiCompareRow({h.urgent_cases: 1, h.scaled_urgent: -1},
                                                             self.treat_threshold),
                                       True: setToFeatureMatrix(h.urgent_satisfied, h.scaled_urgent, 1.0,
                                                                self.treat_threshold),
                                       False: setToFeatureMatrix(h.urgent_satisfied, h.urgent_cases)},
                                False: setToMinMatrix(h.urgent_satisfied, h.urgent_cases, h.total_inventory)}})
        self.world.setDynamics(h.urgent_satisfied, atom, tree)

    def add_allocate_urgent_dynamics(self, atom, h):
        # allocation (treatment) dynamics
        tree = makeTree(KeyedMatrix({h.total_demand: KeyedVector({h.urgent_cases: 1.0, h.non_urgent_cases: 1.0})}))
        self.world.setDynamics(h.total_demand, atom, tree)
        tree = makeTree({'if': differenceRow(h.total_inventory, h.total_demand, self.treat_threshold),
                         True: setToFeatureMatrix(h.urgent_satisfied, h.urgent_cases),
                         False: {'if': greaterThanRow(h.total_inventory, h.urgent_cases),
                                 True: setToFeatureMatrix(h.urgent_satisfied, h.urgent_cases),
                                 False: {'if': thresholdRow(h.total_inventory, 0),
                                         True: setToFeatureMatrix(h.urgent_satisfied, h.total_inventory),
                                         False: setToConstantMatrix(h.urgent_satisfied, 0)}}})
        self.world.setDynamics(h.urgent_satisfied, atom, tree)
        tree = makeTree({'if': differenceRow(h.total_inventory, h.total_demand, self.treat_threshold),
                         True: setToFeatureMatrix(h.non_urgent_satisfied, h.non_urgent_cases),
                         False: {'if': differenceRow(h.total_inventory, h.urgent_cases, self.treat_threshold),
                                 True: multiSetMatrix(h.non_urgent_satisfied,
                                                      {h.total_inventory: 1.0, h.urgent_cases: -1.0,
                                                       CONSTANT: -1.0 * self.treat_threshold}),
                                 False: setToConstantMatrix(h.non_urgent_satisfied, 0)}})
        self.world.setDynamics(h.non_urgent_satisfied, atom, tree)

    def set_reward_functions(self):
        # hospital H1 cares more about Urgent
        self.hospital1.setReward(maximizeFeature(stateKey(self.hospital1.name, 'UrgentSatisfied')), 4.0)
        self.hospital1.setReward(maximizeFeature(stateKey(self.hospital1.name, 'NonUrgentSatisfied')), 1.0)
        self.hospital1.setReward(minimizeFeature(stateKey(self.hospital1.name, 'TotalInventory')), 1.0)

        # hospital H2 cares more about NonUrgent because they make more money from NonUrgent
        self.hospital2.setReward(maximizeFeature(stateKey(self.hospital2.name, 'UrgentSatisfied')), 1.0)
        self.hospital2.setReward(maximizeFeature(stateKey(self.hospital2.name, 'NonUrgentSatisfied')), 4.0)
        self.hospital2.setReward(minimizeFeature(stateKey(self.hospital2.name, 'TotalInventory')), 1.0)

    def add_allocation_state_vars(self, h):
        # many of these would be linked to states in the  simulation for allocation decision
        # syntactic sugar: add properties to agents from state key variables
        h.total_inventory = self.world.defineState(h.name, 'TotalInventory', int, lo=0, hi=self.totals['hInv'])
        h.setState('TotalInventory', 5.0)
        h.up_to_level = self.world.defineState(h.name, 'UpToLevel', int, lo=0, hi=self.totals['hUpTo'])
        h.setState('UpToLevel', 21)
        h.total_on_order = self.world.defineState(h.name, 'TotalOnOrder', int, lo=0, hi=self.totals['hOnOrder'])
        h.setState('TotalOnOrder', 4)
        h.num_up_stream = self.world.defineState(h.name, 'NumUpStream', int, lo=0, hi=self.totals['hUpStr'])
        h.setState('NumUpStream', len(self.suppAgtNames[h.name]))
        h.adj_num_up_stream = self.world.defineState(h.name, 'AdjNumUpStream', float, lo=0, hi=self.totals['hUpStr'])
        h.setState('AdjNumUpStream', len(self.suppAgtNames[h.name]))
        h.order_amount = self.world.defineState(h.name, 'OrderAmount', int, lo=0, hi=self.totals['hOrderAmt'])
        h.setState('OrderAmount', 0)

    def add_ordering_state_vars(self, h):
        # syntactic sugar: add properties to agents from state key variables
        # for order decision
        h.order_to = []
        for j in range(0, len(self.suppAgtNames[h.name])):
            h.order_to.append(
                self.world.defineState(h.name, 'OrderTo' + str(j), int, lo=0, hi=self.totals['hOrderAmt']))
            h.setState('OrderTo' + str(j), 0)

        h.total_demand = self.world.defineState(h.name, 'TotalDemand', int, lo=0, hi=self.totals['hDem'])
        h.setState('TotalDemand', 7)
        h.urgent_cases = self.world.defineState(h.name, 'UrgentCases', int, lo=0, hi=self.totals['hDem'])
        h.setState('UrgentCases', 4)
        h.non_urgent_cases = self.world.defineState(h.name, 'NonUrgentCases', int, lo=0, hi=self.totals['hDem'])
        h.setState('NonUrgentCases', 3)
        h.urgent_satisfied = self.world.defineState(h.name, 'UrgentSatisfied', int, lo=0, hi=self.totals['hDem'])
        h.setState('UrgentSatisfied', 0)
        h.non_urgent_satisfied = self.world.defineState(h.name, 'NonUrgentSatisfied', int, lo=0, hi=self.totals['hDem'])
        h.setState('NonUrgentSatisfied', 0)
        h.supply = self.world.defineState(h.name, 'Supply', int, lo=0.0, hi=100.0)
        h.setState('Supply', 0)
        h.scaled_urgent = self.world.defineState(h.name, 'ScaledUrgent', int, lo=0.0, hi=100.0)
        h.setState('ScaledUrgent', 0.0)
        h.res_inventory = self.world.defineState(h.name, 'ResInventory', int, lo=0.0, hi=100.0)
        h.setState('ResInventory', 0.0)

    def add_allocation_dependencies(self, h):
        # dependency for allocation
        self.world.addDependency(stateKey(h.name, 'NonUrgentSatisfied'), stateKey(h.name, 'TotalDemand'))
        self.world.addDependency(stateKey(h.name, 'NonUrgentSatisfied'), stateKey(h.name, 'Supply'))
        self.world.addDependency(stateKey(h.name, 'ScaledUrgent'), stateKey(h.name, 'TotalDemand'))
        self.world.addDependency(stateKey(h.name, 'ScaledUrgent'), stateKey(h.name, 'Supply'))
        self.world.addDependency(stateKey(h.name, 'UrgentSatisfied'), stateKey(h.name, 'ScaledUrgent'))
        self.world.addDependency(stateKey(h.name, 'ResInventory'), stateKey(h.name, 'UrgentSatisfied'))
        self.world.addDependency(stateKey(h.name, 'ResInventory'), stateKey(h.name, 'NonUrgentSatisfied'))
        self.world.addDependency(stateKey(h.name, 'TotalInventory'), stateKey(h.name, 'ResInventory'))

    def add_ordering_dependencies(self, h):
        # dependency for order
        self.world.addDependency(stateKey(h.name, 'OrderAmount'), stateKey(h.name, 'ResInventory'))
        for j in range(0, len(self.suppAgtNames[h.name])):
            self.world.addDependency(stateKey(h.name, 'OrderTo' + str(j)), stateKey(h.name, 'OrderAmount'))
            self.world.addDependency(stateKey(h.name, 'OrderTo' + str(j)), stateKey(h.name, 'AdjNumUpStream'))

    def run_it(self, Msg):

        print(Msg)
        print("####### Starting State Below ###################")
        print("Bel_Pr Agent           State_Features")
        print("_____________________________________")
        self.world.printState()
        print("####### Starting State Above ###################")
        print("\n")
        # note they act in parallel so only one step
        for t in range(1):
            # level returns amount of details
            self.world.explain(self.world.step(), level=3)
            # print self.world.step()
            # self.world.state[None].select()
            self.world.printState()
            if self.world.terminated():
                break


turnOrder = (set(['H1', 'H2']),)
hospital_agents = SimpHospitalAutoCombineRecipes(turnOrder)

# Create configuration file
config = SafeConfigParser()
f = open('simp_hospital.cfg', 'w')
config.write(f)
f.close()

hospital_agents.run_it("Hospital")
