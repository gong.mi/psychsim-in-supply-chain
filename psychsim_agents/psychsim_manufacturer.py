from .psychsim_agent import PsychSimAgent
from psychsim_agents.constants import *
from .recipes_manufacturer import *
from flow_simulation.classes.decision import *
from psychsim.reward import *
from psychsim.world import World
from .agent_converter import AgentConverter


class PSManufacturerAgent(PsychSimAgent):
    """Defines the states and dynamics for the manufacturer agent"""

    def __init__(self, world, agent_converter, name):
        """
        Creates a PsychSim manufacturer agent with the given name and parameters.
        :param World world: the PsychSim world to which attach the agent.
        :param AgentConverter agent_converter: the correspondence list between PsychSim agent ids and simulation agents.
        :param str name: the name of the agent to be created in PsychSim.
        """
        super(PSManufacturerAgent, self).__init__(world, agent_converter, name)
        self.down_stream_agents = []  # downstream agents of manufacturer
        self.num_down_stream = None

        self.total_inventory = None
        self.inv_after_allocation = None
        self.inv_after_production = None  # inventory after producing products

        self.line_capacity = None  # capacity of production line
        self.num_active_lines = None  # number of working production line
        self.available_capacity = None  # total production available

        # self.last_forecast = None
        # self.forecast_part_one = None
        # self.forecast_part_two = None
        # self.current_forecast = None
        # self.smoothing_coef = None
        self.current_demand = None

        self.up_to_level = None
        # self.lead_time = None

        # backlog includes back order + demand for each agent
        self.total_backlog = None
        self.total_backlog_after_allocation = None
        self.backlog_dict = {}
        self.backlog_after_allocation = {}

        self.allocate_to = {}
        self.temp_allocate_to = {}
        # self.temp_allocate_initial = {}

        self.production_amount = None

        self.total_in_production = None  # work in process (it is in production but not produced yet)
        self.total_inproduction_after_received = None  # updated in-production after product has actually been produced
        self.production_dock = None

        self.production_offset = 0.2
        self.totals = {INV_HI: 400, UP_TO_HI: 400, DN_STR_HI: 3, ORDER_HI: 400, DEM_HI: 400, PROD_HI: 400, LINE_HI: 10,
                       BL_HI: 400, CUR_DEM_HI: 400, LEAD_TIME_HI: 3}

    def create_state_variables(self):
        """
        create PsychSim state variables
        Each agent has to create its own state variables.
        """
        sim_agent = self.agent_converter.get_sim_agent_from_psychsim_agent(self)
        agent_name = self.ps_agent.name

        # defines properties / parameters
        for d in sim_agent.downstream_nodes:
            self.down_stream_agents.append(self.agent_converter.get_psychsim_agent(d))

        # syntactic sugar: add properties to agents from state key variables
        # order state features
        self.num_down_stream = self.world.defineState(agent_name, NUM_DOWN_STREAM, int, lo=0, hi=self.totals[DN_STR_HI])
        self.world.setFeature(self.num_down_stream, len(self.down_stream_agents))

        self.total_inventory = self.world.defineState(agent_name, INVENTORY, int, lo=0, hi=self.totals[INV_HI])
        self.world.setFeature(self.total_inventory, sim_agent.inventory_level())

        self.inv_after_allocation = self.world.defineState(
            agent_name, INV_AFTER_ALLOCATION, int, lo=0.0, hi=self.totals[INV_HI])
        self.world.setFeature(self.inv_after_allocation, 0.0)

        self.inv_after_production = self.world.defineState(
            agent_name, INV_AFTER_PRODUCTION, int, lo=0, hi=self.totals[INV_HI])
        self.world.setFeature(self.inv_after_production, 0)

        self.line_capacity = self.world.defineState(agent_name, LINE_CAPACITY, int, lo=0, hi=self.totals[PROD_HI])
        self.world.setFeature(self.line_capacity, sim_agent.line_capacity)

        self.num_active_lines = self.world.defineState(agent_name, ACTIVE_LINES, int, lo=0, hi=self.totals[LINE_HI])
        self.world.setFeature(self.line_capacity, sim_agent.num_active_lines)

        self.available_capacity = self.world.defineState(
            agent_name, AVAILABLE_CAPACITY, int, lo=0, hi=self.totals[PROD_HI])
        self.world.setFeature(self.available_capacity, sim_agent.num_active_lines * sim_agent.line_capacity)

        self.current_demand = self.world.defineState(agent_name, CURRENT_DEMAND, int, lo=0, hi=self.totals[DEM_HI])
        self.world.setFeature(self.current_demand, 0)

        # self.last_forecast = self.world.defineState(agent_name, LAST_FORECAST, int, lo=0, hi=self.totals[DEM_HI])
        # self.world.setFeature(self.last_forecast, sim_agent.last_forecast)
        #
        # self.forecast_part_one = self.world.defineState(agent_name, FORECAST_ONE, int, lo=0, hi=self.totals[CUR_DEM_HI])
        # self.world.setFeature(self.forecast_part_one, 0)
        #
        # self.forecast_part_two = self.world.defineState(agent_name, FORECAST_TWO, int, lo=0, hi=self.totals[CUR_DEM_HI])
        # self.world.setFeature(self.forecast_part_two, 0)
        #
        # self.current_forecast = self.world.defineState(agent_name, CURRENT_FORECAST, int, lo=0, hi=self.totals[DEM_HI])
        # self.world.setFeature(self.current_forecast, 0)
        #
        # self.smoothing_coef = self.world.defineState(agent_name, SMOOTHING_CONSTANT, float, lo=0, hi=1)
        # self.world.setFeature(self.smoothing_coef, self.smoothing_constant)

        self.up_to_level = self.world.defineState(agent_name, UP_TO_LEVEL, int, lo=0, hi=self.totals[UP_TO_HI])
        self.world.setFeature(self.up_to_level, 0)

        # self.lead_time = self.world.defineState(agent_name, LEAD_TIME, int, lo=0, hi=self.totals[LEAD_TIME_HI])
        # # todo this lead time is the one included in up-to level calculation, can be max or avg of all lead-times
        # self.world.setFeature(self.lead_time, 1)

        self.total_backlog = self.world.defineState(agent_name, TOTAL_BACKLOG, int, lo=0, hi=self.totals[DEM_HI])
        self.world.setFeature(self.total_backlog, sum(bl.amount for bl in sim_agent.backlog))

        self.total_backlog_after_allocation = self.world.defineState(
            agent_name, TOTAL_BACKLOG_AFTER_ALLOCATION, int, lo=0, hi=self.totals[DEM_HI])
        self.world.setFeature(self.total_backlog_after_allocation, 0)

        for d in self.down_stream_agents:
            sim_dnst = self.agent_converter.get_sim_agent_from_psychsim_agent(d)
            self.backlog_dict[d] = self.world.defineState(
                self.name, BACKLOG + str(d), int, lo=0, hi=self.totals[DEM_HI])
            total_agent_demand = 0
            for bl in sim_agent.backlog:
                if bl.src is sim_dnst:
                    total_agent_demand += bl.amount
            self.world.setFeature(self.backlog_dict[d], total_agent_demand)

            self.backlog_after_allocation[d] = self.world.defineState(
                self.name, BACKLOG_AFTER_ALLOCATION + str(d), int, lo=0, hi=self.totals[DEM_HI])
            self.world.setFeature(self.backlog_after_allocation[d], 0)

            self.allocate_to[d] = self.world.defineState(
                self.name, ALLOCATE_TO + str(d), int, lo=0, hi=self.totals[INV_HI])
            self.world.setFeature(self.allocate_to[d], 0)

            self.temp_allocate_to[d] = self.world.defineState(
                self.name, TEMP_ALLOCATE_TO + str(d), int, lo=0, hi=self.totals[INV_HI])
            self.world.setFeature(self.temp_allocate_to[d], 0)

            # self.temp_allocate_initial[c] = self.world.defineState(
            #   agent_name, TEMP_ALLOCATE_INITIAL + c, int, lo=0, hi=self.totals[INV_HI])
            # self.world.setFeature(self.temp_allocate_initial[c], 0)

        self.production_amount = self.world.defineState(
            agent_name, PRODUCTION_AMOUNT, int, lo=0, hi=self.totals[PROD_HI])
        self.world.setFeature(self.production_amount, 0)

        self.total_in_production = self.world.defineState(agent_name, IN_PRODUCTION, int, lo=0, hi=self.totals[PROD_HI])
        self.world.setFeature(self.total_in_production, sum(pr.amount for pr in sim_agent.in_production))

        self.total_inproduction_after_received = self.world.defineState(
            agent_name, IN_PRODUCTION_AFTER_RECEIVED, int, lo=0, hi=self.totals[PROD_HI])
        self.world.setFeature(self.total_inproduction_after_received, 0)

        self.production_dock = (
            self.world.defineState(agent_name, PRODUCTION_DOCK, int, lo=0, hi=self.totals[PROD_HI]))
        self.world.setFeature(self.production_dock, 0)

        # reward
        # self.ps_agent.setReward(minimizeFeature(self.inv_after_allocation), self.inv_after_alloc_rwd_weight)
        # self.ps_agent.setReward(
        #     minimizeFeature(self.total_backlog_after_allocation), self.tot_bklog_after_alloc_rwd_weight)
        # for d in self.down_stream_agents:
        #     self.ps_agent.setReward(maximizeFeature(self.allocate_to[d]), self.allocate_rwd_weight)

    def define_dependencies(self):
        """
        Each agent has to define the dependencies between the state variables.
        Whenever is calculated based on another variable there need to be a dependency defined for the pair.
        """

        # self.world.addDependency(x, y): compute y first, then compute x

        # dependencies for received production
        self.world.addDependency(self.inv_after_production, self.production_dock)
        self.world.addDependency(self.inv_after_production, self.total_inventory)

        self.world.addDependency(self.total_inproduction_after_received, self.production_dock)
        self.world.addDependency(self.total_inproduction_after_received, self.total_in_production)

        # dependencies for allocation recipes
        for d in self.down_stream_agents:
            # self.world.addDependency(self.total_backlog, self.backlog_dict[d])
            # self.world.addDependency(self.temp_allocate_initial[d], self.backlog_dict[d])
            # self.world.addDependency(self.temp_allocate_initial[d], self.total_backlog)
            # self.world.addDependency(self.temp_allocate_to[d], self.inv_after_production)
            # self.world.addDependency(self.temp_allocate_to[d], self.temp_allocate_initial[d])
            # self.world.addDependency(self.temp_allocate_to[d], self.num_down_stream)
            # self.world.addDependency(self.allocate_to[d], self.temp_allocate_to[d])
            # self.world.addDependency(self.allocate_to[d], self.backlog_dict[d])
            self.world.addDependency(self.total_backlog, self.backlog_dict[d])
            self.world.addDependency(self.temp_allocate_to[d], self.inv_after_production)
            self.world.addDependency(self.temp_allocate_to[d], self.num_down_stream)
            self.world.addDependency(self.temp_allocate_to[d], self.backlog_dict[d])
            self.world.addDependency(self.temp_allocate_to[d], self.total_backlog)
            self.world.addDependency(self.allocate_to[d], self.temp_allocate_to[d])
            self.world.addDependency(self.allocate_to[d], self.backlog_dict[d])
            # # dependencies for updating on-order of downstream after allocation decision (for forward projection)
            # dnst_agent = self.ps_agent.world.agents[d]
            # self.world.addDependency(dnst_agent.onorder_after_allocation[agent_name], self.allocate_to[d])

        # dependencies for intermediate state update
        self.world.addDependency(self.inv_after_allocation, self.inv_after_production)
        for d in self.down_stream_agents:
            self.world.addDependency(self.inv_after_allocation, self.allocate_to[d])

        for d in self.down_stream_agents:
            self.world.addDependency(self.backlog_after_allocation[d], self.allocate_to[d])
            self.world.addDependency(self.backlog_after_allocation[d], self.backlog_dict[d])

        # dependencies for demand forecast
        # self.world.addDependency(self.forecast_part_one, self.current_demand)
        # self.world.addDependency(self.forecast_part_one, self.smoothing_coef)
        # self.world.addDependency(self.forecast_part_two, self.last_forecast)
        # self.world.addDependency(self.forecast_part_two, self.smoothing_coef)
        # self.world.addDependency(self.current_forecast, self.forecast_part_one)
        # self.world.addDependency(self.current_forecast, self.forecast_part_two)
        #
        # # dependencies for up-to level
        # self.world.addDependency(self.up_to_level, self.current_forecast)
        # self.world.addDependency(self.up_to_level, self.lead_time)

        # dependencies for production
        for d in self.down_stream_agents:
            self.world.addDependency(self.total_backlog_after_allocation, self.backlog_after_allocation[d])
        self.world.addDependency(self.production_amount, self.up_to_level)
        self.world.addDependency(self.production_amount, self.total_backlog_after_allocation)
        self.world.addDependency(self.production_amount, self.total_inproduction_after_received)
        self.world.addDependency(self.production_amount, self.inv_after_allocation)

    def define_state_dynamics(self, available_recipes):
        """
        Each agent has to define the state variable dynamics according to its own recipes.
        :param dict available_recipes: a dictionary containing the different types of recipes available for the agent.
        """
        update_produced_inventory = ManufacturerReceivedProductionRecipe('prod_inv')
        intermediate_state_update = ManufacturerIntermediateStateUpdateRecipe('int_upd')

        # define the recipes through combination of allocation, production, etc.
        recipe_combinations = PsychSimAgent.get_all_recipe_combinations(available_recipes)
        for recipe_comb in recipe_combinations:

            # creates and adds action with name from all recipes in the combination
            action_name = "-".join([recipe.name for recipe in list(recipe_comb.values())])
            action = self.ps_agent.addAction({'verb': action_name})

            # add produced inventory
            update_produced_inventory.register_recipe(self, action)

            # add allocation recipe
            if ALLOCATION in recipe_comb:
                recipe_comb[ALLOCATION].register_recipe(self, action)

            # intermediate state update
            intermediate_state_update.register_recipe(self, action)

            # add forecasting demand
            if FORECAST_DEMAND in recipe_comb:
                recipe_comb[FORECAST_DEMAND].register_recipe(self, action)

            # add up-to-level calculation
            if CALCULATE_UP_TO_LEVEL in recipe_comb:
                recipe_comb[CALCULATE_UP_TO_LEVEL].register_recipe(self, action)

            # add production recipe
            if PRODUCTION_AMOUNT in recipe_comb:
                recipe_comb[PRODUCTION_AMOUNT].register_recipe(self, action)

    def update_psychsim_from_simulation(self, now):
        """
        Updates the agent's PsychSim state according to the simulation agent state.
        """
        sim_agent = self.agent_converter.get_sim_agent_from_psychsim_agent(self)

        # include if the network changes during the simulation time
        # self.downStreamNames = []
        # for s in sim_agent.downstream_nodes:
        #     # if isinstance(s, Distributor):
        #     if type(s).__name__ == "Distributor":
        #         self.downStreamNames.append('DS_' + str(s.id))
        #     else:
        #         self.downStreamNames.append('HC_' + str(s.id))
        # self.world.setFeature(NUM_DOWN_STREAM, len(self.downStreamNames))
        self.world.setFeature(self.total_inventory, sim_agent.inventory_level())
        self.world.setFeature(self.inv_after_allocation, 0.0)
        self.world.setFeature(self.inv_after_production, 0)

        self.world.setFeature(self.line_capacity, sim_agent.line_capacity)
        self.world.setFeature(self.num_active_lines, sim_agent.num_active_lines)
        self.world.setFeature(self.available_capacity, sim_agent.line_capacity * sim_agent.num_active_lines)

        self.world.setFeature(self.current_demand, sim_agent.demand(now))
        # self.world.setFeature(self.last_forecast, sim_agent.last_forecast)
        # self.world.setFeature(self.forecast_part_one, 0)
        # self.world.setFeature(self.forecast_part_two, 0)
        # self.world.setFeature(self.current_forecast, 0)
        # self.world.setFeature(self.smoothing_coef, self.smoothing_constant)

        self.world.setFeature(self.up_to_level, sim_agent.up_to_level)
        # todo this lead time is the one included in up-to level calculation, can be max or avg of all lead-times
        # self.world.setFeature(self.lead_time, 1)

        self.world.setFeature(self.total_backlog, sum(bl.amount for bl in sim_agent.backlog))
        self.world.setFeature(self.total_backlog_after_allocation, 0)
        for d in self.down_stream_agents:
            total_agent_demand = 0
            sim_dnst = self.agent_converter.get_sim_agent_from_psychsim_agent(d)
            for bl in sim_agent.backlog:
                if bl.src is sim_dnst:
                    total_agent_demand += bl.amount
            self.world.setFeature(self.backlog_dict[d], total_agent_demand)
            self.world.setFeature(self.backlog_after_allocation[d], 0)

            self.world.setFeature(self.allocate_to[d], 0)
            self.world.setFeature(self.temp_allocate_to[d], 0)
            # self.world.setFeature(TEMP_ALLOCATE_INITIAL + c, 0)

        self.world.setFeature(self.production_amount, 0)

        self.world.setFeature(self.total_in_production, sum(pr.amount for pr in sim_agent.in_production))
        self.world.setFeature(self.total_inproduction_after_received, 0)
        self.world.setFeature(self.production_dock, 0)



    def update_simulation_from_psychsim(self, outcomes):
        """
        Updates the simulation agent decisions according to the PsychSim agent's actions.
        """
        sim_agent = self.agent_converter.get_sim_agent_from_psychsim_agent(self)

        # ---produce decision
        # prod_amount = int(round(outcomes[self.production_amount]))
        prod_amount = outcomes[self.production_amount]
        # if prod_amount > 0:
        #     line_cap = sim_agent.line_capacity
        #     prod_amount = ((prod_amount - 1) / line_cap + 1) * line_cap
        decision_p = ProduceDecision()
        decision_p.amount = prod_amount
        sim_agent.prod_amount = prod_amount
        sim_agent.decisions.append(decision_p)

        # -- allocation decision
        all_dict = {}

        for d, amt in self.allocate_to.items():
            sim_dnst = self.agent_converter.get_sim_agent_from_psychsim_agent(d)
            all_dict[sim_dnst] = outcomes[amt]


        if len(sim_agent.inventory) >= 1:
            inv_ptr = 0
            inv_left = sim_agent.inventory[0].amount
            for bl in sim_agent.backlog:
                bl_left = bl.amount
                while (all_dict[bl.src] > 0 and bl_left > 0):
                    if min(all_dict[bl.src], bl_left) <= inv_left:
                        decision_al = AllocateDecision()
                        decision_al.amount = min(all_dict[bl.src], bl_left)
                        all_dict[bl.src] -= decision_al.amount
                        bl_left -= decision_al.amount
                        inv_left -= decision_al.amount
                        decision_al.item = sim_agent.inventory[inv_ptr]
                        decision_al.order = bl
                        sim_agent.decisions.append(decision_al)
                    else:
                        if inv_left > 0:
                            decision_al = AllocateDecision()
                            decision_al.amount = inv_left
                            all_dict[bl.src] -= decision_al.amount
                            bl_left -= decision_al.amount
                            decision_al.item = sim_agent.inventory[inv_ptr]
                            decision_al.order = bl
                            sim_agent.decisions.append(decision_al)
                        inv_ptr += 1
                        if inv_ptr < len(sim_agent.inventory):
                            inv_left = sim_agent.inventory[inv_ptr].amount
                        else:
                            return
