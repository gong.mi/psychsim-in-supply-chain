from flow_simulation.classes.agent import Agent

class AgentConverter(object):
    """
    Represents a converter from PsychSim to simulation agents and vice-versa.
    """

    def __init__(self):
        self._sim_to_ps_agents = {}
        self._ps_to_sim_agents = {}

    def get_sim_agent_from_psychsim_id(self, agent_id):
        """
        Gets the simulation agent given a PsychSim agent's id.
        :param str agent_id: the id / name of the PsychSim agent.
        :return: the corresponding simulation agent.
        :rtype Agent
        """
        return self._ps_to_sim_agents[agent_id]

    def get_sim_agent_from_psychsim_agent(self, agent):
        """
        Gets the simulation agent given a PsychSim agent.
        :param psychsim_agents.psychsim_agent.PsychSimAgent agent: the PsychSim agent.
        :return: the corresponding simulation agent.
        :rtype Agent
        """
        return self._ps_to_sim_agents[agent.ps_agent.name]

    def get_psychsim_agent(self, sim_agent):
        """
        Gets the PsychSim agent given a simulation agent.
        :param Agent sim_agent: the PsychSim agent.
        :return: the corresponding PsychSim agent.
        :rtype psychsim_agents.psychsim_agent.PsychSimAgent
        """
        return self._sim_to_ps_agents[sim_agent]

    def add(self, sim_agent, ps_agent):
        """
        Adds a new agent pair to the converter.
        :param Agent sim_agent: the simulator agent.
        :param psychsim_agents.psychsim_agent.PsychSimAgent ps_agent: the corresponding PsychSim agent.
        """
        self._sim_to_ps_agents[sim_agent] = ps_agent
        self._ps_to_sim_agents[ps_agent.ps_agent.name] = sim_agent

    def clear(self):
        """
        Clears the converter by removing all agents.
        """
        self._ps_to_sim_agents.clear()
        self._sim_to_ps_agents.clear()

    def all_sim_agents(self):
        """
        Gets a list containing all simulation agents.
        :return: a list containing all simulation agents.
        """
        return list(self._sim_to_ps_agents.keys())

    def all_psychsim_agents(self):
        """
        Gets a list containing all PsychSim agents.
        :return: a list containing all PsychSim agents.
        """
        return list(self._sim_to_ps_agents.values())
