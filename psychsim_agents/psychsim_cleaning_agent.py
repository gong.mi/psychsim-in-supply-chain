from .psychsim_agent import PsychSimAgent
from psychsim_agents.constants import *
from psychsim_agents.psychsim_distributor import PSDistributorAgent
from .recipes_cleaning_agent import *


class PSCleaningAgent(PsychSimAgent):
    """Defines the states and dynamics for the cleaning agent
       This agent is responsible for bookkeeping of agents' states during the forward planning
    """
    def __init__(self, world, agent_converter, name):
        """
        Creates a PsychSim distributor agent with the given name and parameters.
        :param World world: the PsychSim world to which attach the agent.
        :param AgentConverter agent_converter: the correspondence list between PsychSim agent ids and simulation agents.
        :param str name: the name of the agent to be created in PsychSim.
        """
        super(PSCleaningAgent, self).__init__(world, agent_converter, name)
        self.other_agents = []
        self.received_dock = {}
        self.expctd_recieved_dock = {}
        self.residual = {}
        self.totals = {ORDER_HI: 1000}

    def create_state_variables(self):

        agent_name = self.ps_agent.name
        for other in self.agent_converter.all_psychsim_agents():
            if other == self:
                continue

            self.other_agents.append(other)
            sim_agt = self.agent_converter.get_sim_agent_from_psychsim_agent(other)

            # if other is PSManufacturerAgent:
            if isinstance(other, PSManufacturerAgent):
                self.received_dock[other] = []
                for t in range(0, sim_agt.lead_time):
                    self.received_dock[other].append(
                        self.world.defineState(
                            agent_name, RECEIVED_DOCK + other.name + TIME + str(t), int, lo=0,
                            hi=self.totals[ORDER_HI]))
                    self.world.setFeature(self.received_dock[other][t], 0)
            else:
                for u in other.up_stream_agents:
                    self.received_dock[other, u] = []
                    sim_upst = self.agent_converter.get_sim_agent_from_psychsim_agent(u)
                    for t in range(0, sim_agt.lead_time_dict[sim_upst]):
                        self.received_dock[other, u].append(
                            self.world.defineState(
                                agent_name, other.name + RECEIVED_DOCK + u.name + TIME + str(t), int, lo=0,
                                hi=self.totals[ORDER_HI]))
                        self.world.setFeature(self.received_dock[other, u][t], 0)

                    self.expctd_recieved_dock[other, u] = []
                    sim_upst = self.agent_converter.get_sim_agent_from_psychsim_agent(u)
                    for t in range(0, sim_agt.lead_time_dict[sim_upst] + 1):
                        self.expctd_recieved_dock[other, u].append(
                            self.world.defineState(
                                agent_name, other.name + EXPCTD_RECIEVED_DOCK + u.name + TIME + str(t), int, lo=0,
                                hi=self.totals[ORDER_HI]))
                        self.world.setFeature(self.expctd_recieved_dock[other, u][t], 0)

                    self.residual[other, u] = self.world.defineState(agent_name, other.name + RESIDUAL + u.name,
                                                                     int, lo=0, hi=self.totals[ORDER_HI])
                    self.world.setFeature(self.residual[other, u], 0)

    def define_dependencies(self):
        """
        Each agent has to define the dependencies between the state variables.
        Whenever is calculated based on another variable there need to be a dependecy defined for the pair.
        """
        for other in self.other_agents:
            self.world.addDependency(other.total_inventory, other.inv_after_allocation)
            if isinstance(other, PSManufacturerAgent):
                for t in range(0, len(self.received_dock[other]) - 1):
                    self.world.addDependency(self.received_dock[other][t], self.received_dock[other][t + 1])
                self.world.addDependency(
                    self.received_dock[other][len(self.received_dock[other]) - 1], other.production_amount)
                self.world.addDependency(other.production_dock, self.received_dock[other][0])
                for d in other.down_stream_agents:
                    self.world.addDependency(other.backlog_dict[d], other.backlog_after_allocation[d])
                    self.world.addDependency(other.backlog_dict[d], d.order_to[other])
                    self.world.addDependency(other.current_demand, d.order_to[other])
                self.world.addDependency(other.total_backlog, other.total_backlog_after_allocation)
                self.world.addDependency(other.total_backlog, other.current_demand)
                self.world.addDependency(other.total_in_production, other.total_inproduction_after_received)
                self.world.addDependency(other.total_in_production, other.production_amount)

            elif isinstance(other, PSDistributorAgent):
                for u in other.up_stream_agents:
                    # received dock dependency
                    for t in range(0, len(self.received_dock[other, u]) - 1):
                        self.world.addDependency(self.received_dock[other, u][t],
                                                 self.received_dock[other, u][t + 1])
                    self.world.addDependency(
                        self.received_dock[other, u][len(self.received_dock[other, u]) - 1], u.allocate_to[other])
                    self.world.addDependency(other.received_dock[u], self.received_dock[other, u][0])
                    # expected received dependency
                    self.world.addDependency(self.residual[other, u], self.expctd_recieved_dock[other, u][0])
                    self.world.addDependency(self.residual[other, u], self.received_dock[other, u][0])

                    self.world.addDependency(self.expctd_recieved_dock[other, u][0],
                                             self.expctd_recieved_dock[other, u][1])
                    self.world.addDependency(self.expctd_recieved_dock[other, u][0],
                                             self.residual[other, u])
                    self.world.addDependency(self.expctd_recieved_dock[other, u][0],
                                             other.order_to[u])
                    for t in range(1, len(self.expctd_recieved_dock[other, u]) - 1):
                        self.world.addDependency(
                            self.expctd_recieved_dock[other, u][t], self.expctd_recieved_dock[other, u][t + 1])
                    self.world.addDependency(
                        self.expctd_recieved_dock[other, u][len(self.expctd_recieved_dock[other, u]) - 1],
                        other.order_to[u])
                    self.world.addDependency(other.expctd_recieved[u], self.expctd_recieved_dock[other, u][0])

                    self.world.addDependency(other.on_order_dict[u], other.onorder_after_received[u])
                    self.world.addDependency(other.on_order_dict[u], other.order_to[u])

                    self.world.addDependency(other.ontime_deliv[u], other.received_dock[u])
                    self.world.addDependency(other.ontime_deliv[u], other.expctd_recieved[u])

                for d in other.down_stream_agents:
                    self.world.addDependency(other.backlog_dict[d], other.backlog_after_allocation[d])
                    self.world.addDependency(other.backlog_dict[d], d.order_to[other])
                    # self.world.addDependency(other.current_demand, d.order_to[other])
                self.world.addDependency(other.total_backlog, other.total_backlog_after_allocation)
                # self.world.addDependency(other.total_backlog, other.current_demand)
            else:
                for u in other.up_stream_agents:
                    # received dock dependency
                    for t in range(0, len(self.received_dock[other, u]) - 1):
                        self.world.addDependency(self.received_dock[other, u][t], self.received_dock[other, u][t + 1])
                    self.world.addDependency(
                        self.received_dock[other, u][len(self.received_dock[other, u]) - 1], u.allocate_to[other])
                    self.world.addDependency(other.received_dock[u], self.received_dock[other, u][0])
                    # expected received dependency
                    for t in range(0, len(self.expctd_recieved_dock[other, u]) - 1):
                        self.world.addDependency(
                            self.expctd_recieved_dock[other, u][t], self.expctd_recieved_dock[other, u][t + 1])
                    self.world.addDependency(
                        self.expctd_recieved_dock[other, u][len(self.expctd_recieved_dock[other, u]) - 1],
                        other.order_to[u])
                    self.world.addDependency(other.expctd_recieved[u], self.expctd_recieved_dock[other, u][0])

                    self.world.addDependency(other.on_order_dict[u], other.onorder_after_received[u])
                    self.world.addDependency(other.on_order_dict[u], other.order_to[u])

                    self.world.addDependency(other.ontime_deliv[u], other.received_dock[u])
                    self.world.addDependency(other.ontime_deliv[u], other.expctd_recieved[u])

    def update_psychsim_from_simulation(self, now):
        """
        Updates the agent's PsychSim state according to the simulation agent state.
        """
        for other in self.other_agents:
            sim_agt = self.agent_converter.get_sim_agent_from_psychsim_agent(other)
            # if other is PSManufacturerAgent:
            if isinstance(other, PSManufacturerAgent):
                for t in range(0, sim_agt.lead_time):
                    self.world.setFeature(self.received_dock[other][t], sim_agt.in_transit_inventory[sim_agt][t])
            else:
                for u in other.up_stream_agents:
                    sim_upst = self.agent_converter.get_sim_agent_from_psychsim_agent(u)
                    for t in range(0, sim_agt.lead_time_dict[sim_upst]):
                        self.world.setFeature(
                            self.received_dock[other, u][t], sim_agt.in_transit_inventory[sim_upst][t])
                        self.world.setFeature(self.expctd_recieved_dock[other, u][t],
                                              sim_agt.expctd_on_order[sim_upst][t])

    def define_state_dynamics(self, available_recipes):
        """
        Each agent has to define the state variable dynamics according to its own recipes.
        :param dict available_recipes: a dictionary containing the different types of recipes available for the agent.
        """
        generate_patient = GeneratePatients('gen_patient')
        update_received_docks = UpdatingReceivedDocks('upd_rcv_docks')
        update_other_features = UpdateOtherFeatures('upd_other_feat')
        action = self.ps_agent.addAction({'verb': 'clean'})
        generate_patient.register_recipe(self, action)
        update_received_docks.register_recipe(self, action)
        update_other_features.register_recipe(self, action)
