""" All trust, allocation and ordering recipes for psychsim distributor """

from .recipe import Recipe
from .helper_functions import *
from .constants import *
from psychsim.action import Action


class DistributorReceiveShipmentRecipe(Recipe):
    """Utility recipe for updating received inventory based on received dock"""

    def register_recipe(self, agent, action):
        """
        Registers the recipe in the action.
        :param psychsim_agents.psychsim_distributor.PSDistributorAgent agent: the agent for which to register the recipe.
        :param Action action: the action for which the recipe is going to be added.
        """
        # update total inventory
        update_inv_dict = {agent.total_inventory: 1.0}
        for s in agent.up_stream_agents:
            update_inv_dict[agent.received_dock[s]] = 1.0
        tree = makeTree(multiSetMatrix(agent.inv_after_received, update_inv_dict))
        agent.world.setDynamics(agent.inv_after_received, action, tree)

        # deduct from on-order of upstream
        for s in agent.up_stream_agents:
            tree = makeTree(
                multiSetMatrix(agent.onorder_after_received[s],
                               {agent.on_order_dict[s]: 1.0, agent.received_dock[s]: -1.0}))
            agent.world.setDynamics(agent.onorder_after_received[s], action, tree)


class DistributorUpdateTrustByHistoryRecipe(Recipe):
    # pass
    """Updates the trust that an agent is associated to its upstream nodes based on placed order and received order"""

    def __init__(self, name="Recipe", delta=0.5):
        super(DistributorUpdateTrustByHistoryRecipe, self).__init__(name)
        self.delta = delta

    def register_recipe(self, agent, action):
        """
        Registers the recipe in the action.
        :param psychsim_agents.psychsim_distributor.PSDistributorAgent agent: the agent for which to register the recipe.
        :param Action action: the action for which the recipe is going to be added.
        """
        for s in agent.up_stream_agents:
            tree = makeTree(multiSetMatrix(agent.trust[s],
                                           {agent.trust[s]: 1 - self.delta, agent.ontime_deliv[s]: self.delta}))
            agent.world.setDynamics(agent.trust[s], action, tree)


class DistributorAllocateEquallyRecipe(Recipe):
    def register_recipe(self, agent, action):
        """
        Registers the recipe in the action.
        :param psychsim_agents.psychsim_distributor.PSDistributorAgent agent: the agent for which to register the recipe.
        :param Action action: the action for which the recipe is going to be added.
        """
        tree = makeTree({'if': thresholdRow(agent.inv_after_received, 0),
                         True: setToScaledKeyMatrix(agent.equal_allocate, agent.inv_after_received, 1.0,
                                                    agent.num_down_stream, .01),
                         False: setToConstantMatrix(agent.equal_allocate, 0)})
        agent.world.setDynamics(agent.equal_allocate, action, tree)

        for d in agent.down_stream_agents:
            tree = makeTree({'if': greaterThanRow(agent.equal_allocate, agent.backlog_dict[d]),
                             True: setToFeatureMatrix(agent.temp_allocate_to[d], agent.backlog_dict[d]),
                             False: setToFeatureMatrix(agent.temp_allocate_to[d], agent.equal_allocate)})
            agent.world.setDynamics(agent.temp_allocate_to[d], action, tree)

        temp_allocate_to_dict = {}
        for d in agent.down_stream_agents:
            temp_allocate_to_dict[agent.temp_allocate_to[d]] = -1
        temp_allocate_to_dict[agent.inv_after_received] = 1

        tree = makeTree(multiSetMatrix(agent.extra_allocate, temp_allocate_to_dict))
        agent.world.setDynamics(agent.extra_allocate, action, tree)

        # to make sure allocation amount is not more than demand
        for d in agent.down_stream_agents:
            tree = makeTree({'if': KeyedPlane(KeyedVector(
                {agent.extra_allocate: 1., agent.temp_allocate_to[d]: 1, agent.backlog_dict[d]: -1.}), 0),
                True: setToFeatureMatrix(agent.allocate_to[d], agent.backlog_dict[d]),
                False: multiSetMatrix(agent.allocate_to[d], {agent.extra_allocate: 1., agent.temp_allocate_to[d]: 1})})
            agent.world.setDynamics(agent.allocate_to[d], action, tree)


class DistributorAllocateProportionalRecipe(Recipe):
    def register_recipe(self, agent, action):
        """
        Registers the recipe in the action.
        :param psychsim_agents.psychsim_distributor.PSDistributorAgent agent: the agent for which to register the recipe.
        :param Action action: the action for which the recipe is going to be added.
        """
        # total backlog
        update_bl_dict = {}
        for d in agent.down_stream_agents:
            update_bl_dict[agent.backlog_dict[d]] = 1.0
        tree = makeTree(multiSetMatrix(agent.total_backlog, update_bl_dict))
        agent.world.setDynamics(agent.total_backlog, action, tree)
        # sample_1 = get_bivariate_samples(division_func, 0, agent.totals[BL_HI], 0, agent.totals[BL_HI],
        #                                  agent.totals[BL_HI]/10, agent.totals[BL_HI]/10)
        # sample_2 = get_bivariate_samples(multiply_func, 0, agent.totals[INV_HI], 0, 1.1,
        #                                 agent.totals[INV_HI]/10, 110)
        for d in agent.down_stream_agents:
            tree = makeTree({'if': thresholdRow(agent.inv_after_received, 0),
                             True: setToScaledKeyMatrix(agent.temp_allocate_to[d], agent.inv_after_received,
                                                        agent.backlog_dict[d], agent.total_backlog,
                                                        .01),
                             False: setToConstantMatrix(agent.temp_allocate_to[d], 0)})
            agent.world.setDynamics(agent.temp_allocate_to[d], action, tree)
            # tree_1=makeTree(tree_from_bivariate_samples(agent.temp_allocate_initial[d], agent.backlog_dict[d],
            #                                                    agent.total_backlog, sample_1, 0, agent.totals[BL_HI]/10-1,0, agent.totals[BL_HI]/10-1))
            # agent.world.setDynamics(agent.temp_allocate_initial[d], action, tree_1)
            #
            #
            # tree_2 = makeTree({'if': thresholdRow(agent.inv_after_received, 0),
            #                  True: tree_from_bivariate_samples(agent.temp_allocate_to[d], agent.inv_after_received,
            #                                                    agent.temp_allocate_initial[d], sample_2, 0, agent.totals[INV_HI]/10-1, 0, 109),
            #                  False: setToConstantMatrix(agent.temp_allocate_to[d], 0)})
            # agent.world.setDynamics(agent.temp_allocate_to[d], action, tree_2)
        # to make sure allocation amount is not more than demand
        for d in agent.down_stream_agents:
            tree = makeTree({'if': greaterThanRow(agent.temp_allocate_to[d], agent.backlog_dict[d]),
                             True: setToFeatureMatrix(agent.allocate_to[d], agent.backlog_dict[d]),
                             False: setToFeatureMatrix(agent.allocate_to[d], agent.temp_allocate_to[d])})
            agent.world.setDynamics(agent.allocate_to[d], action, tree)


class DistributorIntermediateStateUpdateRecipe(Recipe):
    """Utility recipe to aggregate ordering and allocation policies in Manufacturer agents"""

    def register_recipe(self, agent, action):
        """
        Registers the recipe in the action.
        :param psychsim_agents.psychsim_distributor.PSDistributorAgent agent: the agent for which to register the recipe.
        :param Action action: the action for which the recipe is going to be added.
        """
        # dummy variable for forcing PsychSim to use updated inventory when producing
        update_inv_dict = {agent.inv_after_received: 1.0}
        for d in agent.down_stream_agents:
            update_inv_dict[agent.allocate_to[d]] = -1.0
        tree = makeTree(multiSetMatrix(agent.inv_after_allocation, update_inv_dict))
        agent.world.setDynamics(agent.inv_after_allocation, action, tree)

        # update backlog
        for d in agent.down_stream_agents:
            tree = makeTree(
                multiSetMatrix(agent.backlog_after_allocation[d],
                               {agent.backlog_dict[d]: 1.0, agent.allocate_to[d]: -1.0}))
            agent.world.setDynamics(agent.backlog_after_allocation[d], action, tree)


class DistributorDemandForecastRecipe(Recipe):
    def register_recipe(self, agent, action):
        """
        Registers the recipe in the action.
        :param psychsim_agents.psychsim_distributor.PSDistributorAgent agent: the agent for which to register the recipe.
        :param Action action: the action for which the recipe is going to be added.
        """
        pass
        # samples_1 = get_bivariate_samples(multiply_func, 0, agent.totals[CUR_DEM_HI], 0, 1, agent.totals[CUR_DEM_HI], 100)
        # tree_1 = makeTree(
        #     tree_from_bivariate_samples(agent.forecast_part_one, agent.current_demand, agent.smoothing_coef, samples_1,
        #                                 0, agent.totals[CUR_DEM_HI]-1, 0, 99))
        # agent.world.setDynamics(agent.forecast_part_one, action, tree_1)
        # samples_2 = get_bivariate_samples(multiply_minus_one_func, 0, agent.totals[CUR_DEM_HI], 0, 1, agent.totals[CUR_DEM_HI], 100)
        # tree_2 = makeTree(
        #     tree_from_bivariate_samples(agent.forecast_part_two, agent.last_forecast, agent.smoothing_coef, samples_2,
        #                                 0, agent.totals[CUR_DEM_HI]-1, 0, 99))
        # agent.world.setDynamics(agent.forecast_part_two, action, tree_2)
        # samples_3 = get_bivariate_samples(add_func, 0, agent.totals[CUR_DEM_HI], 0, agent.totals[CUR_DEM_HI], agent.totals[CUR_DEM_HI], agent.totals[CUR_DEM_HI])
        # tree_3 = makeTree(
        #     tree_from_bivariate_samples(agent.current_forecast, agent.forecast_part_one, agent.forecast_part_two,
        #                                 samples_3,
        #                                 0, agent.totals[CUR_DEM_HI]-1, 0, agent.totals[CUR_DEM_HI]-1))
        # agent.world.setDynamics(agent.current_forecast, action, tree_3)


class DistributorUpToLevelRecipe(Recipe):
    def register_recipe(self, agent, action):
        """
        Registers the recipe in the action.
        :param psychsim_agents.psychsim_distributor.PSDistributorAgent agent: the agent for which to register the recipe.
        :param Action action: the action for which the recipe is going to be added.
        """
        pass
        # samples = get_bivariate_samples(multiply_func, 0, agent.totals[CUR_DEM_HI], 0, agent.totals[LEAD_TIME_HI], agent.totals[CUR_DEM_HI],
        #                                 agent.totals[LEAD_TIME_HI])
        # tree = makeTree(
        #     tree_from_bivariate_samples(agent.up_to_level, agent.current_forecast, agent.lead_time, samples, 0, agent.totals[CUR_DEM_HI]-1, 0,
        #                                 agent.totals[LEAD_TIME_HI]-1))
        #
        # agent.world.setDynamics(agent.up_to_level, action, tree)


class DistributorUpToOrderAmountRecipe(Recipe):
    def __init__(self, name="Recipe", offset=0.0):
        super(DistributorUpToOrderAmountRecipe, self).__init__(name)
        self.offset = offset

    def register_recipe(self, agent, action):
        """
        Registers the recipe in the action.
        :param psychsim_agents.psychsim_distributor.PSDistributorAgent agent: the agent for which to register the recipe.
        :param Action action: the action for which the recipe is going to be added.
        """
        # total backlog
        update_bl_dict = {}
        for d in agent.down_stream_agents:
            update_bl_dict[agent.backlog_after_allocation[d]] = 1.0
        tree = makeTree(multiSetMatrix(agent.total_backlog_after_allocation, update_bl_dict))
        agent.world.setDynamics(agent.total_backlog_after_allocation, action, tree)
        # total on-order
        update_onorder_dict = {}
        for s in agent.up_stream_agents:
            update_onorder_dict[agent.onorder_after_received[s]] = 1.0
        tree = makeTree(multiSetMatrix(agent.total_on_order, update_onorder_dict))
        agent.world.setDynamics(agent.total_on_order, action, tree)
        # splits order total offset by trust
        offset_factor = 1.0 + self.offset
        tree = makeTree(
            {'if': multiCompareRow({agent.up_to_level: offset_factor,
                                    agent.total_backlog_after_allocation: offset_factor,
                                    agent.inv_after_allocation: -offset_factor,
                                    agent.total_on_order: -offset_factor}),
             False: setToConstantMatrix(agent.order_amount, 0),
             True: multiSetMatrix(agent.order_amount,
                                  {agent.up_to_level: offset_factor,
                                   agent.total_backlog_after_allocation: offset_factor,
                                   agent.inv_after_allocation: -offset_factor,
                                   agent.total_on_order: -offset_factor})})
        agent.world.setDynamics(agent.order_amount, action, tree)


class DistributorOrderSplitByTrustRecipe(Recipe):
    def register_recipe(self, agent, action):
        pass


#         """
#         Registers the recipe in the action.
#         :param psychsim_agents.psychsim_distributor.PSDistributorAgent agent: the agent for which to register the recipe.
#         :param Action action: the action for which the recipe is going to be added.
#         """
#         coef_dict = {}
#         for s in agent.up_stream_agents:
#             coef_dict[agent.trust[s]] = 1.0
#         tree = makeTree(multiSetMatrix(agent.sum_coef, coef_dict))
#         agent.world.setDynamics(agent.sum_coef, action, tree)
#
#         samples_2 = get_bivariate_samples(div, 0, 1, 0, 2, 100, 200)
#         for s in agent.up_stream_agents:
#             tree = makeTree(tree_from_bivariate_samples(agent.final_coef[s],
#                                                         agent.trust[s], agent.sum_coef,
#                                                         samples_2, 0, 99, 0, 199))
#             agent.world.setDynamics(agent.final_coef[s], action, tree)
#
#         max_o = agent.totals[ORDER_HI]
#         num_samples = max_o
#         samples_3 = get_bivariate_samples(mult, 0, max_o, 0, 1, num_samples, 100)
#         for s in agent.up_stream_agents:
#             tree = makeTree(tree_from_bivariate_samples(agent.order_to[s],
#                                                         agent.order_amount, agent.final_coef[s],
#                                                         samples_3, 0, num_samples - 1, 0, 99))
#
#             agent.world.setDynamics(agent.order_to[s], action, tree)


class DistributorOrderSplitEquallyRecipe(Recipe):
    def register_recipe(self, agent, action):
        """
        Registers the recipe in the action.
        :param psychsim_agents.psychsim_distributor.PSDistributorAgent agent: the agent for which to register the recipe.
        :param Action action: the action for which the recipe is going to be added.
        """
        num_upstream = len(agent.up_stream_agents)
        for s in agent.up_stream_agents:
            tree = makeTree(multiSetMatrix(agent.order_to[s], {agent.order_amount: 1.0 / num_upstream}))
            agent.world.setDynamics(agent.order_to[s], action, tree)

        # # samples = get_bivariate_samples(division_func, 0, agent.totals[ORDER_HI], 0, agent.totals[UP_STR_HI],
        # #                                 agent.totals[ORDER_HI]/10, agent.totals[UP_STR_HI])
        # for s in agent.up_stream_agents:
        #     tree = makeTree({'if': thresholdRow(agent.order_amount, 0),
        #                      True: setToScaledKeyMatrix(agent.order_to[s], agent.order_amount, 1.0, agent.num_up_stream,
        #                                                 .01),
        #                      False: setToConstantMatrix(agent.order_to[s], 0)})
        #     # tree = makeTree({'if': thresholdRow(agent.order_amount, 0),
        #     #                  True: tree_from_bivariate_samples(agent.order_to[s], agent.order_amount,
        #     #                                                           agent.num_up_stream, samples, 0, agent.totals[ORDER_HI]/10 - 1, 0, agent.totals[UP_STR_HI]-1),
        #     #                  False: setToConstantMatrix(agent.order_to[s], 0)})
        #     agent.world.setDynamics(agent.order_to[s], action, tree)


class DistributorAllocateAgentPreferOneRecipe(Recipe):
    def register_recipe(self, agent, action):
        h1 = agent.down_stream_agents[0]
        h2 = agent.down_stream_agents[1]

        tree = makeTree({'if': differenceRow(agent.inv_after_received, agent.backlog_dict[h1], 0),
                         True: setToFeatureMatrix(agent.allocate_to[h1], agent.backlog_dict[h1]),
                         False: setToFeatureMatrix(agent.allocate_to[h1], agent.inv_after_received)})
        agent.world.setDynamics(agent.allocate_to[h1], action, tree)

        tree = makeTree({'if': differenceRow(agent.inv_after_received, agent.backlog_dict[h1], 0),
                         True: multiSetMatrix(agent.new_inv_1,
                                              {agent.inv_after_received: 1.0, agent.backlog_dict[h1]: -1.0}),
                         False: setToConstantMatrix(agent.new_inv_1, 0)})
        agent.world.setDynamics(agent.new_inv_1, action, tree)

        tree = makeTree({'if': differenceRow(agent.new_inv_1, agent.backlog_dict[h2], 0),
                         True: setToFeatureMatrix(agent.allocate_to[h2], agent.backlog_dict[h2]),
                         False: setToFeatureMatrix(agent.allocate_to[h2], agent.new_inv_1)})
        agent.world.setDynamics(agent.allocate_to[h2], action, tree)


class DistributorAllocateAgentPreferTwoRecipe(Recipe):
    def register_recipe(self, agent, action):
        h1 = agent.down_stream_agents[0]
        h2 = agent.down_stream_agents[1]

        tree = makeTree({'if': differenceRow(agent.inv_after_received, agent.backlog_dict[h2], 0),
                         True: setToFeatureMatrix(agent.allocate_to[h2], agent.backlog_dict[h2]),
                         False: setToFeatureMatrix(agent.allocate_to[h2], agent.inv_after_received)})
        agent.world.setDynamics(agent.allocate_to[h2], action, tree)

        tree = makeTree({'if': differenceRow(agent.inv_after_received, agent.backlog_dict[h2], 0),
                         True: multiSetMatrix(agent.new_inv_2,
                                              {agent.inv_after_received: 1.0, agent.backlog_dict[h2]: -1.0}),
                         False: setToConstantMatrix(agent.new_inv_2, 0)})
        agent.world.setDynamics(agent.new_inv_2, action, tree)

        tree = makeTree({'if': differenceRow(agent.new_inv_2, agent.backlog_dict[h1], 0),
                         True: setToFeatureMatrix(agent.allocate_to[h1], agent.backlog_dict[h1]),
                         False: setToFeatureMatrix(agent.allocate_to[h1], agent.new_inv_2)})
        agent.world.setDynamics(agent.allocate_to[h1], action, tree)
